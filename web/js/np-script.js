var items;
var cityRefForDepartment;
var cityRefForStreet;
var streetRef;
var departmentRef;
var departmentNumber;

function getCityRefs() {
    $("#city").autocomplete({
        autoFocus: true,
        source: function(request, response) {
            $.ajax({
                url: "/np-ajax/city",
                dataType: "json",
                data: {
                    term : request.term,
                },
                success: function(data,type) {
                    items = data;
                    response(items);
                },
                error: function(data,type){
                    console.log( type);
                    $('.hidden-alert-danger').css('display', 'block');

                },
            });
        },
        select: function (event, ui) {
            event.preventDefault();
            $(this).val(ui.item.label);

            cityRefForDepartment = ui.item.forDepartment;
            cityRefForStreet = ui.item.forStreet;

            $('#npform-npservicetype').removeAttr('disabled');
        }
    });
}

function department(ref) {
    $.ajax({
        url: "/np-ajax/departments",
        dataType: "json",
        data: {
            city : ref,
        },
        success: function(data) {
            response_arr = data;

            console.log(data);
            $('#department').html('');
            $('#department').append(
                $("<option></option>")

                    .text('- Выберите отделение -')

            );
            response_arr.forEach(function (item, i, response_arr) {
                $('#department')
                    .append(
                        $("<option></option>")
                            .attr("value", item.value)
                            .text(item.description)
                            .attr("data-number", item.number)
                    )
            });
        },
        error: function(data,type){
            console.log( type);
        }
    });

    $('#department').on('change', function() {
        var str = "";
        $( "#department option:selected" ).each(function() {
            departmentRef = $(this).attr('value');
            departmentNumber =$(this).attr('data-number');

            $('#np-department-ref').val(departmentRef);
            $('#np-department-num').val(departmentNumber);
        });

    });
}

function clearFields() {
    $('#department, #street, #npform-nprecipienthouse, #npform-nprecipientflat').val();
}

function street(ref) {
    $("#street").autocomplete({
        autoFocus: true,
        source: function(request, response) {
            $.ajax({
                url: "/np-ajax/streets",
                dataType: "json",
                data: {
                    term : request.term,
                    city : ref
                },
                success: function(data,type) {
                    items = data;
                    //console.log(data);
                    response(items);
                },
                error: function(data,type){
                    console.log( type);
                },
            });
        },
        select: function (event, ui) {
            event.preventDefault();
            $(this).val(ui.item.label);
            streetRef = ui.item.value;
            //console.log(street_ref);
        }
    });
}

$(document).ready(function() {
    getCityRefs();

    $('#npform-npservicetype').change(function () {
        // console.log(cityRefForDepartment);
        // console.log(cityRefForStreet);
        selectVal = $(this).val();
        console.log(selectVal);

        if (selectVal == "WarehouseWarehouse") {
            clearFields();
            $('#department, #npform-npweight, #npform-npvolumegeneral, #npform-npseatsamount, #npform-npdescription, #npform-nppayertype, #npform-nppaymentmethod, #npform-npreturnshippingtype, #np-department-ref, #np-city-name, #np-department-num').removeAttr('disabled');
            $('#street, #npform-nprecipienthouse, #npform-nprecipientflat').attr('disabled', true);
            department(cityRefForDepartment);
        } else if (selectVal == "WarehouseDoors") {
            clearFields();
            $('#department').attr('disabled', true);
            $('#street, #npform-nprecipienthouse, #npform-nprecipientflat, #npform-npweight, #npform-npvolumegeneral, #npform-npseatsamount, #npform-npdescription, #npform-nppayertype, #npform-nppaymentmethod, #npform-npreturnshippingtype, #np-department-ref, #np-city-name, #np-department-num').removeAttr('disabled');
            street(cityRefForStreet);
        } else if (selectVal !== 'WarehouseWarehouse' || selectVal !== 'WarehouseDoors') {
            clearFields();
            $('#department, #street, #npform-nprecipienthouse, #npform-nprecipientflat, #npform-npweight, #npform-npvolumegeneral, #npform-npseatsamount, #npform-npdescription, #npform-nppayertype, #npform-nppaymentmethod, #npform-npreturnshippingtype, #np-department-ref, #np-city-name, #np-department-num').attr('disabled', true);
        }
    });

    $('#npform-npreturnshippingtype').on('change', function () {
        returnVal = $(this).val();
        console.log(returnVal);

        if (returnVal == 'Other') {
            $('#npform-npreturnshippingcomment').removeAttr('disabled');
            $('#npform-npreturnshippingcomment').attr('required', true);
        } else {
            $('#npform-npreturnshippingcomment').attr('disabled', true);
            $('#npform-npreturnshippingcomment').removeAttr('required');
        }
    });
});
