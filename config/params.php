<?php

return [
    'adminEmail' => 'admin@example.com',
    'novaPoshta' => [
        'apiKey'        => '6045e2e8a37175909f4884392c5205c8',
        'modelName'     => 'InternetDocument',
        'calledMethod'  => 'save',
        'NewAddress'    => '1',
        'CitySender'    => 'db5c88f0-391c-11dd-90d9-001a92567626',
        'Sender'        => 'b941a714-80f0-11e7-ab8b-005056b2fc3d',
        'SenderAddress' => '0d545f56-e1c2-11e3-8c4a-0050568002cf',
        'ContactSender' => '413a8b6d-81b0-11e7-8ba8-005056881c6b',
        'SendersPhone'  => '380730046266',
    ]
];