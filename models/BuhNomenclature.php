<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "buh_nomenclature".
 *
 * @property string $id
 * @property string $name
 * @property string $storage_type_id
 */
class BuhNomenclature extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'buh_nomenclature';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'storage_type_id'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID товара',
            'name' => 'Название',
            'storage_type_id' => 'Склад',
        ];
    }
    
    public static function getTovarNameById($id)
    {
        $tovarName = self::find()->asArray()->select('name')->where(['id' => $id])->one();
        return ArrayHelper::getValue($tovarName, 'name');
    }    
}
