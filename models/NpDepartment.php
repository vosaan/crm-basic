<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "np_department".
 *
 * @property string $id
 * @property string $city_id
 * @property string $short_description
 * @property string $description
 * @property integer $number
 */
class NpDepartment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'np_department';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'city_id', 'short_description', 'description', 'number'], 'safe'],
            [['number'], 'integer'],
            [['id', 'city_id'], 'string', 'max' => 50],
            [['description', 'short_description'], 'string', 'max' => 150],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'city_id' => 'City ID',
            'description' => 'Description',
            'short_description' => 'Short Description',
            'number' => 'Number',
        ];
    }
}
