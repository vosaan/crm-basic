<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "np_ttn".
 *
 * @property integer $id
 * @property integer $lead_id
 * @property string $city
 * @property integer $department
 * @property string $street
 * @property string $house
 * @property integer $flat
 * @property string $ttn_number
 * @property string $estimated_delivery_date
 * @property integer $cost
 * @property integer $np_status_code
 * @property string $np_status
 * @property string $payer_type
 * @property string $payment_method
 * @property string $delivery_tech
 */
class NpTtn extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'np_ttn';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lead_id', 'city', 'payer_type', 'payment_method', 'delivery_tech'], 'required'],
            [['ttn_number', 'cost', 'estimated_delivery_date', 'np_status'], 'safe'],
            [['lead_id', 'department', 'flat', 'ttn_number', 'cost', 'np_status_code'], 'integer'],
            [['city', 'np_status', 'payer_type', 'payment_method', 'delivery_tech'], 'string', 'max' => 255],
            [['street'], 'string', 'max' => 100],
            [['house'], 'string', 'max' => 5],
            [['estimated_delivery_date'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'lead_id' => 'Lead ID',
            'city' => 'City',
            'department' => 'Department',
            'street' => 'Street',
            'house' => 'House',
            'flat' => 'Flat',
            'ttn_number' => 'Ttn Number',
            'estimated_delivery_date' => 'Estimated Delivery Date',
            'cost' => 'Cost',
            'np_status_code' => 'Np Status Code',
            'np_status' => 'Np Status',
            'payer_type' => 'Payer Type',
            'payment_method' => 'Payment Method',
            'delivery_tech' => 'Delivery Tech',
        ];
    }

    public static function getTtnNumberByLeadId($id){
        $data = self::find()->where(['lead_id' => $id])->asArray()->one();
        return ArrayHelper::getValue($data, 'ttn_number');
    }
}
