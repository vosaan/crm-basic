<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "np_ttn_error".
 *
 * @property integer $id
 * @property integer $lead_id
 * @property string $registered_time
 * @property string $reason
 */
class NpTtnError extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'np_ttn_error';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lead_id', 'reason'], 'required'],
            [['lead_id'], 'integer'],
            [['registered_time'], 'safe'],
            [['reason'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'lead_id' => 'Lead ID',
            'registered_time' => 'Registered Time',
            'reason' => 'Reason',
        ];
    }
}
