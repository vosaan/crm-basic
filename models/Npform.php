<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 14.09.2017
 * Time: 14:32
 */

namespace app\models;
use yii\base\Model;

class NpForm extends Model
{
    public $npPayerType;
    public $npPaymentMethod;
    public $npReturnShippingType;
    public $npReturnShippingComment;
    public $npVolumeGeneral;
    public $npWeight;
    public $npServiceType;
    public $npSeatsAmount;
    public $npDescription;
    public $npRecipient;
    public $npRecipientCityName;
    public $npRecipientArea;
    public $npRecipientAreaRegions;
    public $npRecipientAddressName;
    public $npRecipientHouse;
    public $npRecipientFlat;
//    public $npRecipientName;
//    public $npRecipientsPhone;
//    public $npDateTime;


    public function rules()
    {
        return [
            [
                [
                    'npRecipient',
                    'npRecipientCityName',
                    'npRecipientArea',
                    'npRecipientAreaRegions',
                    'npRecipientAddressName',
                    'npRecipientHouse',
                    'npRecipientFlat',
                    'npReturnShippingComment'
                ],
                'safe'],
            [
                [
                    'npPayerType',
                    'npPaymentMethod',
                    'npReturnShippingType',
                    'npVolumeGeneral',
                    'npWeight',
                    'npServiceType',
                    'npSeatsAmount',
                    'npDescription',
            ],
                'required']
        ];
    }
}