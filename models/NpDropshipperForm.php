<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 06.10.2017
 * Time: 14:35
 */

namespace app\models;
use yii\base\Model;

class NpDropshipperForm extends Model
{
    public $city;
    public $department;
    public $street;
    public $house;
    public $flat;
    public $payerType;
    public $paymentMethod;
    public $deliveryTech;
    public $ttnNumber;

    public function rules(){
        return [
            [['city', 'payerType', 'paymentMethod', 'deliveryTech'], 'required'],
            [['department', 'street', 'house', 'flat', 'ttnNumber'], 'safe'],
        ];
    }
}