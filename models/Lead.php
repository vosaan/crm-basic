<?php

namespace app\models;

use Yii;
use app\models\NpTtn;

/**
 * This is the model class for table "lead".
 *
 * @property integer $id
 * @property string $title
 * @property integer $product_id
 * @property string $name
 * @property string $second_name
 * @property string $phone
 * @property string $email
 * @property integer $price
 * @property integer $purchase_price
 * @property string $domain
 * @property string $vcode
 * @property integer $status_id
 * @property integer $responsible_user_id
 * @property string $comment
 * @property string $date_time
 * @property string $quantity
 */
class Lead extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lead';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'name', 'phone', 'price', 'domain'], 'required'],
            [['product_id', 'price', 'status_id', 'responsible_user_id', 'quantity'], 'integer'],
            [['comment'], 'string'],
            [['purchase_price'], 'double'],
            [['date_time', 'purchase_price'], 'safe'],
            [['title', 'name', 'second_name'], 'string', 'max' => 255],
            [['phone'], 'string', 'max' => 20],
            [['email', 'domain'], 'string', 'max' => 100],
            [['vcode'], 'string', 'max' => 50],
            [['second_name', 'quantity'], 'required', 'on' => 'update'],
        ];
    }
    
    public function getnp_ttn()
    {
        return $this->hasOne(NpTtn::className(), ['lead_id' => 'id']);
    }

    public function getTtn() {
        return $this->np_ttn->ttn_number;
    }

    public function getTtnStatus() {
        return $this->np_ttn->np_status;
    }

    public function getCity() {
        return $this->np_ttn->city;
    }

    public function getproduct()
    {
        return $this->hasOne(BuhNomenclature::className(), ['id' => 'product_id']);
    }

    public function getStorageid() {
        return $this->product->storage_type_id;
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID лида',
            'title' => 'Название товара',
            'product_id' => 'ID товара',
            'name' => 'Имя заказчика',
            'second_name' => 'Фамилия заказчика',
            'phone' => 'Номер телефона заказчика',
            'email' => 'E-mail заказчика',
            'price' => 'Цена товара',
            'domain' => 'Адрес лендинга',
            'vcode' => 'vcode (для Octotracker)',
            'status_id' => 'Статус лида',
            'responsible_user_id' => 'ID оператора, отвестсвенного за лид',
            'comment' => 'Комметратий к лиду',
            'date_time' => 'Дата и время поступления лида',
            'ttn' => 'Номер ЭН',
            'ttnStatus' => 'Статус посылки',
            'city' => 'Населенный пункт',
        ];
    }
}
