<?php

namespace app\models;

use yii\web\IdentityInterface;
use yii\db\ActiveRecord;
use Yii;
use yii\helpers\ArrayHelper;

class User extends ActiveRecord implements IdentityInterface
{
    /** Устанавливает имя таблицы, с котороу работает модель
     * @return string
     */
    public static function tableName()
    {
        return 'user';
    }

   /** Метода проверяет наличие пользователя в БД с именем, которое пришло из формы из поля username
     * @param $login
     * @return static
     */
    public static function findByUsername($login)
    {
        return self::findOne(['login' => $login]);
    }

    /** В БД пароль хранится в виде хэша. Метод возвращает хэш строки, которая пришла из формы из поля password
     * @return string
     */
    public function getSecurePassword()
    {
        return Yii::$app->getSecurity()->generatePasswordHash($this->password);
    }

    public function setPassword($password)
    {
        $this->password = Yii::$app->security->generatePasswordHash($password);
    }

    /** Метод сравнивает хэш паролья из формы с хэшем пароля из БД
     * @param $password
     * @return bool
     */
    public function checkUserPassword($password){
        return Yii::$app->getSecurity()->validatePassword($password, $this->password);
    }

    /* Методы интерфейса IdentityInterface, которые не надо менять, но надо объявить*/

    public static function findIdentity($id)
    {
        return self::findOne($id);
    }

    public function getId()
    {
        return $this->id;
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {

    }

    public function getAuthKey()
    {

    }

    public function validateAuthKey($authKey)
    {

    }

    /** Возвращает роль пользователя по его ID
     * @param $id
     * @return mixed
     */
    public static function getUserRoleByUserId($id)
    {
        $userRole = AuthAssignment::find()->asArray()->select('item_name')->where(['user_id' => $id])->one();
        return ArrayHelper::getValue($userRole, 'item_name');
    }
}
