<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 16.06.2017
 * Time: 10:42
 */

namespace app\models;
use yii\base\Model;
use Yii;

class LoginForm extends Model
{
    public $login;
    public $password;

    /** Правила валидации для формы логина
     * @return array
     */
    public function rules()
    {
        return [
            [['login', 'password'], 'required'],
            [['login', 'password'], 'string', 'length' => [3, 25]],
            //['password', 'validatePassword']
        ];
    }

    /** Задаёт значения элементов <label> в форме авторизации
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'login' => 'Login',
            'password' => 'Password',
        ];
    }

    /**
     * Кастомный метод валидации, проверяет существование прользователя и соответствие пароля
     */
    public function validatePassword()
    {
        $user = User::findByUsername($this->login);

        if (!$user || !$user->checkUserPassword($this->password)){
            $this->addError('password', 'Wrong login or password');
        }
    }

    public function userExist(){
        $user = User::findByUsername($this->login);
        if (!$user || !$user->checkUserPassword($this->password)){
            return false;
        } else {
            return true;
        }
    }

    /** Метод логинит пользователя
     * @return bool
     */
    public function login(){
        return Yii::$app->user->login(User::findByUsername($this->login));
    }

}