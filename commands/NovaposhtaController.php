<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 29.08.2017
 * Time: 12:03
 */

namespace app\commands;

use app\helpers\NpHelper;
use app\helpers\Storage;
use app\helpers\Utils;
use yii\console\Controller;
use app\models\NpPayerType;
use app\models\NpPaymentMethod;
use app\models\NpArea;
use app\models\NpCity;
use app\models\NpDeliveryTech;
use app\models\NpDepartment;
use yii\helpers\ArrayHelper;
use app\models\NpReturnShippingType;
use app\models\NpTtn;
use app\models\Lead;
use Yii;

class NovaposhtaController extends Controller
{
    /** Обновляет список типов плательщиков
     *
     */
    public function actionUpdatePayerType(){
        /* Удаление всех предыдущих записей */
        NpPayerType::deleteAll();

        /* Добавление актуальных записей */
        $url = "http://api.novaposhta.ua/v2.0/common/getTypesOfPayers/json";
        $paramsArray = [
            "modelName" => "Common",
            "calledMethod" => "getTypesOfPayers",
            "apiKey" => Yii::$app->params['novaPoshta']['apiKey']
        ];

        $result = Utils::CurTolNP($url, json_encode($paramsArray));
        $resultObj = json_decode($result);

        foreach ($resultObj->data as $payerType){
            $npPayerType = new NpPayerType();
            $npPayerType->id = $payerType->Ref;
            $npPayerType->name = $payerType->Description;
            $npPayerType->save();
        }
    }

    /** Обновляет список способов оплаты
     *
     */
    public function actionUpdatePaymentMethod(){
        /* Удаление всех предыдущих записей */
        NpPaymentMethod::deleteAll();

        /* Добавление актуальных записей */
        $url = "http://api.novaposhta.ua/v2.0/common/getPaymentForms/json";
        $paramsArray = [
            "modelName" => "Common",
            "calledMethod" => "getPaymentForms",
            "apiKey" => Yii::$app->params['novaPoshta']['apiKey']
        ];

        $result = Utils::CurTolNP($url, json_encode($paramsArray));
        $resultObj = json_decode($result);

        foreach ($resultObj->data as $paymentMethod){
            $npPaymentMethod = new NpPaymentMethod();
            $npPaymentMethod->id = $paymentMethod->Ref;
            $npPaymentMethod->name = $paymentMethod->Description;
            $npPaymentMethod->save();
        }
    }

    /** Обновляет список областей Украины
     *
     */
    public function actionUpdateArea(){
        /* Удаление всех предыдущих записей */
        NpArea::deleteAll();

        /* Добавление актуальных записей */
        $url = "http://api.novaposhta.ua/v2.0/json/Address/getAreas";
        $paramsArray = [
            "modelName" => "Address",
            "calledMethod" => "getAreas",
            "apiKey" => Yii::$app->params['novaPoshta']['apiKey']
        ];

        $result = Utils::CurTolNP($url, json_encode($paramsArray));
        $resultObj = json_decode($result);

        foreach ($resultObj->data as $area){
            $npArea = new NpArea();
            $npArea->id = $area->Ref;
            $npArea->name = $area->Description;
            $npArea->save();
        }
    }

    /** Обновляет список населенных пунктов, в которых есть отделения НП
     *
     */
    public function actionUpdateCity(){
        /* Удаление всех предыдущих записей */
        NpCity::deleteAll();

        /* Добавление актуальных записей */
        $url = "http://api.novaposhta.ua/v2.0/json/Address/getCities";
        $paramsArray = [
            "modelName" => "Address",
            "calledMethod" => "getCities",
            "apiKey" => Yii::$app->params['novaPoshta']['apiKey']
        ];

        $result = Utils::CurTolNP($url, json_encode($paramsArray));
        $resultObj = json_decode($result);

        foreach ($resultObj->data as $city){
            $npCity = new NpCity();
            $npCity->id = $city->Ref;

            //$npArea = NpArea::find()->where(['id' => $city->Area])->asArray()->one();
            //$npAreaName = ArrayHelper::getValue($npArea,'name');

            //$npCity->name = $city->Description . ", " . $city->SettlementTypeDescription . ", " . $npAreaName . " обл.";
            $npCity->name = $city->DescriptionRu;
            $npCity->save();
        }
    }

    /** Обновляет список технологий доставки
     *
     */
    public function actionUpdateDeliveryTech(){
        /* Удаление всех предыдущих записей */
        NpDeliveryTech::deleteAll();

        /* Добавление актуальных записей */
        $url = "http://api.novaposhta.ua/v2.0/common/getServiceTypes/json";
        $paramsArray = [
            "modelName" => "Common",
            "calledMethod" => "getServiceTypes",
            "apiKey" => Yii::$app->params['novaPoshta']['apiKey']
        ];

        $result = Utils::CurTolNP($url, json_encode($paramsArray));
        $resultObj = json_decode($result);

        foreach ($resultObj->data as $tech){
            $npDeliveryTech = new NpDeliveryTech();
            $npDeliveryTech->id = $tech->Ref;
            $npDeliveryTech->name = $tech->Description;
            $npDeliveryTech->save();
        }
    }

    /** Обновляет список отделений в населенных пунктах
     *
     */
    public function actionUpdateDepartments(){
        /* Удаление всех предыдущих записей */
        NpDepartment::deleteAll();

        /* Получение id всех городов */
        $cities = NpCity::find()->all();
        $citiesRefs = ArrayHelper::getColumn($cities, 'id');
        sort($citiesRefs);

        /* Добавление актуальных записей */
        $i = 1;
        foreach ($citiesRefs as $city) {
            $url = "http://api.novaposhta.ua/v2.0/json/AddressGeneral/getWarehouses";
            $paramsArray = [
                "modelName" => "AddressGeneral",
                "calledMethod" => "getWarehouses",
                "methodProperties" => [
                    "CityRef" => $city
                ],
                "apiKey" => Yii::$app->params['novaPoshta']['apiKey']
            ];

            echo "___" . $i++ . "___  " . $city . "\n";

            $result = Utils::CurTolNP($url, json_encode($paramsArray));
            $resultObj = json_decode($result);


            if ($result) {
                Utils::fOut($resultObj);
                foreach ($resultObj->data as $department){
//                    echo "___" . $i++ . "___  " . $department->CityRef . "\n";
                    $npDepartment = new NpDepartment();
                    $npDepartment->id = $department->Ref;
                    $npDepartment->city_id = $department->CityRef;
                    $npDepartment->description = $department->DescriptionRu;
                    $npDepartment->number = $department->Number;
                    $npDepartment->short_description = $department->CityDescriptionRu;
                    $npDepartment->save();
                }
            }

            sleep(1);
        }
    }

    public function actionUpdateReturnShippingType(){
        /* Удаление всех предыдущих записей */
        NpReturnShippingType::deleteAll();

        /* Добавление актуальных записей */
        $url = "http://api.novaposhta.ua/v2.0/common/getBackwardDeliveryCargoTypes/json";
        $paramsArray = [
            "modelName" => "Common",
            "calledMethod" => "getBackwardDeliveryCargoTypes",
            "apiKey" => Yii::$app->params['novaPoshta']['apiKey']
        ];

        $result = Utils::CurTolNP($url, json_encode($paramsArray));
        $resultObj = json_decode($result);

        foreach ($resultObj->data as $type){
            $npReturnShippingType = new NpReturnShippingType();
            $npReturnShippingType->id = $type->Ref;
            $npReturnShippingType->name = $type->Description;
            $npReturnShippingType->save();
        }
    }

    public function actionTestCity(){
        $url = "http://api.novaposhta.ua/v2.0/json/AddressGeneral/getWarehouses";
        $paramsArray = [
            "modelName" => "AddressGeneral",
            "calledMethod" => "getWarehouses",
            "methodProperties" => [
                "CityRef" => "7833e624-3b51-11de-913b-001d92f7869"
            ],
            "apiKey" => Yii::$app->params['novaPoshta']['apiKey']
        ];

        $result = Utils::CurTolNP($url, json_encode($paramsArray, JSON_UNESCAPED_UNICODE));
        $resultObj = json_decode($result);

        Utils::fOut($resultObj);
    }

    public function actionCron(){
        /* Обновление статусов */

        /* Выбрать все накладные, статус которых отличается от 11 (Відправлення отримано. Грошовий переказ видано одержувачу) */
        $ttns = NpTtn::find()->where('np_status_code != :np_status_code', ['np_status_code'=>11])->asArray()->all();
        //$ttns = NpTtn::find()->asArray()->all();

        //Utils::fOut($ttns, 1);

        /* Получить простой массив номеров ТТН  */
        $ttnNumbersArray  = self::getTtnArray($ttns);

        /* Получить статусы переданных ТТН */
        $statuses = NpHelper::getTtnStatus($ttnNumbersArray);

        /* Обновить статусы для каждой ТТН */
        foreach ($statuses as $ttn) {
            /* Создать модель ТТН на основании номера ТТН */
            $model = NpTtn::find()->where(['ttn_number' => $ttn['ttn_number']])->one();

            /* Изменить и сохранить статус ТТН */
            $model->np_status = $ttn['np_status'];
            $model->np_status_code = $ttn['np_status_code'];
            $model->save();

            /* Если статус 11 (Відправлення отримано. Грошовий переказ видано одержувачу), изменить статус лида и
            произвести списание со склада */
            if ($ttn['np_status_code'] == 11) {

                /* Изменение статуса лида на "Выкуплен" */
                $lead = Lead::findOne($model->lead_id);
                $lead->status_id = 6;
                $lead->save();

                /* Списание со склада (товар списывается только если он на основном складе)*/
                if ($lead->storageid == 1) {
                    Storage::debitStorageLead($lead->product_id, $lead->quantity, $lead->id);
                }
            }
            /* Если статус больше 3 (4 - послка на отделении или сскладе в городе-отправителе) и не равно 11
            (Відправлення отримано. Грошовий переказ видано одержувачу), присвоить лиду статус "В пути"*/
            elseif ($ttn['np_status_code'] > 3 && $ttn['np_status_code'] !== 11) {

                /* Изменение статуса лида на "В пути"*/
                $lead = Lead::findOne($model->lead_id);
                $lead->status_id = 5;
                $lead->save();
            }
        }
    }

    public function actionTest(){
        $file =  __DIR__ . '/../web/file.txt';
        $f = fopen($file, 'w+');
        fwrite($f, time());
        fclose($f);
        echo "Okay";
    }

    /**
     * @param $ttn_number
     * @return array|bool|null|\yii\db\ActiveRecord
     */
    protected function findModel($ttn_number)
    {
        if (($model = NpTtn::find()->where(['ttn_number' => $ttn_number])->one()) !== null) {
            return $model;
        } else {
            return false;
        }
    }

    private static function getTtnArray($ttns){
        $numbers = [];

        foreach ($ttns as $ttn) {
            $numbers[] = $ttn['ttn_number'];
        }

        return $numbers;
    }
}