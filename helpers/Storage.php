<?php
namespace app\helpers;
use Yii;

class Storage
{
    public static function debitStorageLead($product_id, $quantity, $lead_id){
        
        $summ = self::debitStorage($product_id, $quantity);
        
        Yii::$app->db->createCommand("INSERT INTO `buh_debit` (`id`, `date_time`, `product_id`, `quantity`, `amount`, `comment`, `lead_id`) VALUES (NULL, CURRENT_TIMESTAMP, '".$product_id . "', '" . $quantity . "', '" . $summ . "', 'На основании лида №" . $lead_id . "', '" . $lead_id . "')")->execute();
        
        return true;
    }
    
    public static function debitStorage($product_id, $quantity){
        $post = Yii::$app->db->createCommand('SELECT `quantity`, `price_total` FROM buh_storage WHERE product_id=:id')
                   ->bindValue(':id', $product_id)
                   ->queryOne();
        $priceOneProduct = $post['price_total'] / $post['quantity'];
        $priceProduct = $priceOneProduct * $quantity;
        $newTotal = $post['price_total'] - $priceProduct;
        $newQuantity = $post['quantity'] - $quantity;
        Yii::$app->db->createCommand('UPDATE `buh_storage` SET `quantity` = ' . $newQuantity . ', `price_total` = ' . $newTotal . ' WHERE product_id ='  . $product_id)->execute();
        
        return $priceProduct;
    }
    
    public static function creditStorage($product_id, $quantity, $amount){
        $post = Yii::$app->db->createCommand('SELECT `quantity`, `price_total` FROM buh_storage WHERE product_id=:id')
                   ->bindValue(':id', $product_id)
                   ->queryOne();
                
        $newTotal = $post['price_total'] + $amount;
        $newQuantity = $post['quantity'] + $quantity;
        
        Yii::$app->db->createCommand('UPDATE `buh_storage` SET `quantity` = ' . $newQuantity . ', `price_total` = ' . $newTotal . ' WHERE product_id ='  . $product_id)->execute();
        
        return true;
    }    
}