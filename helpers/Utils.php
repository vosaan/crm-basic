<?php
namespace app\helpers;

use app\models\NpArea;

class Utils
{
    public static function fOut($data, $die=0)
    {
        echo "<pre>" . print_r($data, 1) . "</pre>";
        if ($die) die;
    }

    public static function CurTolNP($url, $params){

        if( $curl = curl_init() ) {
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
            curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $params);
            curl_setopt($curl, CURLOPT_HTTPHEADER, [
                'Content-Type: application/json',
                'Content-Length: ' . strlen($params)
            ]);

            $out = curl_exec($curl);

            if($out === false){
                return false;
            }else{
                curl_close($curl);
                return $out;
            }
        }
    }

    public static function CurTolNPXml($url, $params){

        if( $curl = curl_init() ) {
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_TIMEOUT, 10);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $params);
            curl_setopt($curl, CURLOPT_HTTPHEADER, [
                "Content-type: text/xml",
                "Content-length: " . strlen($params),
                "Connection: close"
            ]);

            $out = curl_exec($curl);

            if($out === false){
                return false;
            }else{
                curl_close($curl);
                return $array_data = json_decode(json_encode(simplexml_load_string($out)), true);;
            }
        }
    }

    public static function preparePhoneNumber($phone){
        $number = trim($phone, '+');
        $number = str_replace('-', '', $number);
        $number = str_replace('(', '', $number);
        $number = str_replace(')', '', $number);
        return $number;
    }

    public static function prepareCityName($cityName){
        return $result = trim(strstr($cityName, '(', true));
    }

    public static function getAreaByCityName($city){
        $city = stristr($city, '(');
        $city = trim($city, '(');
        $areaName = stristr($city, ' ', true);
        return $areaName;
    }

    public static function getRegionByCityName($city){
        $region = $city = stristr($city, ',');
        $region = trim(trim($region, ', '), ')');
        $region = stristr($region, ' ', true);
        return $region;
    }
}