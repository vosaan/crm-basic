<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 20.09.2017
 * Time: 10:15
 */

namespace app\helpers;
use Yii;
use app\models\NpTtn;
use app\models\NpTtnError;
use yii\helpers\ArrayHelper;

class NpHelper
{
    public static function toDepartment($lead, $npFormData, $city){

        $xmlParams = "
            <root>
            <apiKey>" . Yii::$app->params['novaPoshta']['apiKey'] . "</apiKey>
            <modelName>InternetDocument</modelName>
            <calledMethod>save</calledMethod>
                <methodProperties>
                    <NewAddress>1</NewAddress>
                    <PayerType>" . $npFormData->npPayerType . "</PayerType>
                    <PaymentMethod>" . $npFormData->npPaymentMethod . "</PaymentMethod>
                    <CargoType>Cargo</CargoType>
                    <VolumeGeneral>" . $npFormData->npVolumeGeneral . "</VolumeGeneral>
                    <Weight>" . $npFormData->npWeight . "</Weight>
                    <ServiceType>" . $npFormData->npServiceType . "</ServiceType>
                    <SeatsAmount>" . $npFormData->npSeatsAmount . "</SeatsAmount>
                    <Description>" . $npFormData->npDescription . "</Description>
                    <Cost>" . $lead->price ."</Cost>
                    <CitySender>" . Yii::$app->params['novaPoshta']['CitySender'] ."</CitySender>
                    <Sender>" . Yii::$app->params['novaPoshta']['Sender'] . "</Sender>
                    <SenderAddress>" . Yii::$app->params['novaPoshta']['SenderAddress'] . "</SenderAddress>
                    <ContactSender>" . Yii::$app->params['novaPoshta']['ContactSender'] . "</ContactSender>
                    <SendersPhone>" . Yii::$app->params['novaPoshta']['SendersPhone'] . "</SendersPhone>
                    <Recipient>" . $npFormData->npRecipient . "</Recipient>
                    <RecipientCityName>" . Utils::prepareCityName($city) . "</RecipientCityName>
                    <RecipientArea></RecipientArea>
                    <RecipientAreaRegions></RecipientAreaRegions>
                    <RecipientAddressName>" . $npFormData->npRecipientAddressName . "</RecipientAddressName>
                    <RecipientHouse></RecipientHouse>
                    <RecipientFlat></RecipientFlat>
                    <RecipientName>" . $lead->name . " " . $lead->second_name . "</RecipientName>
                    <RecipientType>PrivatePerson</RecipientType>
                    <RecipientsPhone>" . Utils::preparePhoneNumber($lead->phone) . "</RecipientsPhone>
                    <DateTime>" . date('d.m.Y') . "</DateTime>
                </methodProperties>
            </root>
        ";


        /* ВНИМАНИЕ! КОСТЫЛЬ! Если указана обратная доставка, то в xml-строке </methodProperties> заменяется на соотв.
           кусок, в конце которого тоже дописывается </methodProperties>.*/
        if ($npFormData->npReturnShippingType == 'Money') {
            $backwardDelivery = "
                    <BackwardDeliveryData>
                        <item>
                            <CargoType>Money</CargoType>
                            <PayerType>" . $npFormData->npPayerType . "</PayerType>
                            <RedeliveryString>" . $lead->price . "</RedeliveryString>
                        </item>
                    </BackwardDeliveryData>
                </methodProperties>            
            ";

            $xmlParams = str_replace('</methodProperties>', $backwardDelivery, $xmlParams);

        } elseif ($npFormData->npReturnShippingType == 'Other') {
            $backwardDelivery = "
                    <BackwardDeliveryData>
                        <item>
                            <CargoType>Other</CargoType>
                            <PayerType>" . $npFormData->npPayerType . "</PayerType>
                            <RedeliveryString>" . $npFormData->npReturnShippingComment . "</RedeliveryString>
                        </item>
                    </BackwardDeliveryData>
                </methodProperties>            
            ";

            $xmlParams = str_replace('</methodProperties>', $backwardDelivery, $xmlParams);
        }

        $url = "https://api.novaposhta.ua/v2.0/xml/";

        $result = Utils::CurTolNPXml($url, $xmlParams);

        $npReport = [];
        $npReport['success'] = $result['success'];
        $npReport['number'] = $result['data']['item']['IntDocNumber'];
        $npReport['cost'] = $result['data']['item']['CostOnSite'];
        $npReport['estimatedDeliveryDate'] = $result['data']['item']['EstimatedDeliveryDate'];

        /* Если накладная не создалась (ошибка) */
        if ($result['success'] == 'false') {
            $npReport['errors'] = $result['errors'];
        }

        return $npReport;
    }

    public static function toAddress($lead, $npFormData, $city, $street){
        $xmlParams = "
            <root>
            <apiKey>" . Yii::$app->params['novaPoshta']['apiKey'] . "</apiKey>
            <modelName>InternetDocument</modelName>
            <calledMethod>save</calledMethod>
                <methodProperties>
                    <NewAddress>1</NewAddress>
                    <PayerType>" . $npFormData->npPayerType . "</PayerType>
                    <PaymentMethod>" . $npFormData->npPaymentMethod . "</PaymentMethod>
                    <CargoType>Cargo</CargoType>
                    <VolumeGeneral>" . $npFormData->npVolumeGeneral . "</VolumeGeneral>
                    <Weight>" . $npFormData->npWeight . "</Weight>
                    <ServiceType>" . $npFormData->npServiceType . "</ServiceType>
                    <SeatsAmount>" . $npFormData->npSeatsAmount . "</SeatsAmount>
                    <Description>" . $npFormData->npDescription . "</Description>
                    <Cost>" . $lead->price ."</Cost>
                    <CitySender>" . Yii::$app->params['novaPoshta']['CitySender'] ."</CitySender>
                    <Sender>" . Yii::$app->params['novaPoshta']['Sender'] . "</Sender>
                    <SenderAddress>" . Yii::$app->params['novaPoshta']['SenderAddress'] . "</SenderAddress>
                    <ContactSender>" . Yii::$app->params['novaPoshta']['ContactSender'] . "</ContactSender>
                    <SendersPhone>" . Yii::$app->params['novaPoshta']['SendersPhone'] . "</SendersPhone>                   
                    <RecipientCityName>" . Utils::prepareCityName($city) . "</RecipientCityName>
                    <RecipientArea>" . Utils::getAreaByCityName($city) . "</RecipientArea>
                    <RecipientAreaRegions>" . Utils::getRegionByCityName($city) . "</RecipientAreaRegions>
                    <RecipientAddressName>" . $street . "</RecipientAddressName>
                    <RecipientHouse>" . $npFormData->npRecipientHouse . "</RecipientHouse>
                    <RecipientFlat>" . $npFormData->npRecipientFlat . "</RecipientFlat>
                    <RecipientName>" . $lead->name . " " . $lead->second_name . "</RecipientName>
                    <RecipientType>PrivatePerson</RecipientType>
                    <RecipientsPhone>" . Utils::preparePhoneNumber($lead->phone) . "</RecipientsPhone>
                    <DateTime>" . date('d.m.Y') . "</DateTime>
                </methodProperties>
            </root>
        ";


        /* ВНИМАНИЕ! КОСТЫЛЬ! Если указана обратная доставка, то в xml-строке </methodProperties> заменяется на соотв.
           кусок, в коце которого тоже допичывается </methodProperties>.*/
        if ($npFormData->npReturnShippingType == 'Money') {
            $backwardDelivery = "
                    <BackwardDeliveryData>
                        <item>
                            <CargoType>Money</CargoType>
                            <PayerType>" . $npFormData->npPayerType . "</PayerType>
                            <RedeliveryString>" . $lead->price . "</RedeliveryString>
                        </item>
                    </BackwardDeliveryData>
                </methodProperties>            
            ";

            $xmlParams = str_replace('</methodProperties>', $backwardDelivery, $xmlParams);

        } elseif ($npFormData->npReturnShippingType == 'Other') {
            $backwardDelivery = "
                    <BackwardDeliveryData>
                        <item>
                            <CargoType>Other</CargoType>
                            <PayerType>" . $npFormData->npPayerType . "</PayerType>
                            <RedeliveryString>" . $npFormData->npReturnShippingComment . "</RedeliveryString>
                        </item>
                    </BackwardDeliveryData>
                </methodProperties>            
            ";

            $xmlParams = str_replace('</methodProperties>', $backwardDelivery, $xmlParams);
        }

        $url = "https://api.novaposhta.ua/v2.0/xml/";

        $result = Utils::CurTolNPXml($url, $xmlParams);

        $npReport = [];
        $npReport['success'] = $result['success'];
        $npReport['number'] = $result['data']['item']['IntDocNumber'];
        $npReport['cost'] = $result['data']['item']['CostOnSite'];
        $npReport['estimatedDeliveryDate'] = $result['data']['item']['EstimatedDeliveryDate'];

        /* Если накладная не создалась (ошибка) */
        if ($result['success'] == 'false') {
            $npReport['errors'] = $result['errors'];
        }

        return $npReport;
    }

    public static function saveTtn($leadId, $npData, $npFormData, $city, $street = null){
        $model = new NpTtn();

        $model->lead_id = $leadId;
        $model->city = $city;
        $model->department = $npFormData->npRecipientAddressName;
        $model->street = $street;
        $model->house = $npFormData->npRecipientHouse;
        $model->flat = $npFormData->npRecipientFlat;
        $model->ttn_number = $npData['number'];
        $model->estimated_delivery_date = $npData['estimatedDeliveryDate'];
        $model->cost = $npData['cost'];
        $model->np_status_code = 1;
        $model->np_status = "Нова пошта очікує надходження від відправника";
        $model->payer_type = $npFormData->npPayerType;
        $model->payment_method = $npFormData->npPaymentMethod;
        $model->delivery_tech = $npFormData->npServiceType;
        $model->save();

        return "Ok";
    }

    public static function saveTtnError($leadId, $npData){
        $errorMessage = '';

        $errors = $npData['errors']['item'];

        if (!is_array($errors)) {
            $errorMessage = $errors;
        } else {
            foreach ($npData['errors']['item'] as $error) {
                if (!is_array($error) && $error !== '') {
                    $errorMessage .= trim($error, '.') . ". ";
                }
            }
        }

        $model = new NpTtnError();
        $model->lead_id = $leadId;
        $model->reason = $errorMessage;
        $model->save();

        return $errorMessage;
    }

    public static function getTtnStatus($ttnNumbersArray){
        /* Поученные номера ТТН будут обрабатываться одним запрсом */

        $xmlParams = "
            <root>
               <apiKey>" . Yii::$app->params['novaPoshta']['apiKey'] . "</apiKey>
               <calledMethod>getStatusDocuments</calledMethod>
               <methodProperties>
                  <Documents>  
                  </Documents>
               </methodProperties>
               <modelName>TrackingDocument</modelName>
            </root>
        ";

        $items = '';

        foreach ($ttnNumbersArray as $number) {
            $items .= "
                <item>
                    <DocumentNumber>" . $number . "</DocumentNumber>
                    <Phone></Phone>
                </item>
            ";
        }

        $items .= "</Documents>";

        $xmlParams = str_replace('</Documents>', $items, $xmlParams);

        $url = "https://api.novaposhta.ua/v2.0/xml/";

        $result = Utils::CurTolNPXml($url, $xmlParams);

        $statuses = [];

        $ttns = $result['data']['item'];

        /* Небольшой костыль: если происходит обработка нескольких накладных, то в $result['data']['item'] будет
        индексированный массив, где каждому индексу будут соответствовать ассоциативный массив с данными о
        накладных. Иначе (обработка одной накладной) в $result['data']['item'] будет ассоциативный массив с данными об
        одной накладной */
        if (ArrayHelper::isIndexed($ttns)) {
            $i = 0;
            foreach ($ttns as $ttn) {
                $statuses[$i]['ttn_number'] = $ttn['Number'];
                $statuses[$i]['np_status'] = $ttn['Status'];
                $statuses[$i]['np_status_code'] = $ttn['StatusCode'];
                $i++;
            }
        } else {
            $statuses[0]['ttn_number'] = $ttns['Number'];
            $statuses[0]['np_status'] = $ttns['Status'];
            $statuses[0]['np_status_code'] = $ttns['StatusCode'];
        }

        return $statuses;
    }
}