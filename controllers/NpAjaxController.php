<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 13.09.2017
 * Time: 17:40
 */

namespace app\controllers;
use app\helpers\Utils;
use yii\helpers\Html;
use yii\web\Controller;
use Yii;

class NpAjaxController extends Controller
{
    /* Онлайн-поиск населенных пунктов с отделениями Новой почты */
    public function actionCity(){
        if ($get = \Yii::$app->request->get()) {
            $needle = Html::encode($get['term']);

            $url = 'https://api.novaposhta.ua/v2.0/json/';
            $paramsArray = [
                "modelName" => "Address",
                "calledMethod" => "searchSettlements",
                "methodProperties" => [
                    "CityName" => $needle,
                    "Limit" => "10",
                ],
                "apiKey" => Yii::$app->params['novaPoshta']['apiKey']
            ];

            $result = Utils::CurTolNP($url, json_encode($paramsArray));
            $resultObj = json_decode($result);

            $cities = [];
            $i=0;
            foreach ($resultObj->data[0]->Addresses as $city) {

                $cities[$i]['value'] = $city->Ref;
                $cities[$i]['label'] = $city->MainDescription . " (" . $city->Area . " обл., " . $city->Region . " р-н)";
                $cities[$i]['forStreet'] = $city->Ref;
                $cities[$i]['forDepartment'] = $city->DeliveryCity;
                $i++;
            }

            echo json_encode($cities,JSON_UNESCAPED_UNICODE);

        }
    }

    /* Онлайн-поиск отделений Новой почты по конкретному городу*/
    public function actionDepartments(){
        if ($get = \Yii::$app->request->get()) {
            $needle = $get['city'];
            $url = "https://api.novaposhta.ua/v2.0/json/";
            $paramsArray = [
                "modelName" => "AddressGeneral",
                "calledMethod" => "getWarehouses",
                "methodProperties" => [
                    "CityRef" => $needle
                ],
                "apiKey" => Yii::$app->params['novaPoshta']['apiKey']
            ];

            $result = Utils::CurTolNP($url, json_encode($paramsArray));
            $resultObj = json_decode($result);



            $departments = [];
            $i=0;
            foreach ($resultObj->data as $department) {
                $departments[$i]['value'] = $department->Ref;
                $departments[$i]['description'] = $department->DescriptionRu;
                $departments[$i]['number'] = $department->Number;
                $i++;
            }

            echo json_encode($departments, JSON_UNESCAPED_UNICODE);
        }
    }

    /* Онлайн-поиск улиц по конкретному городу*/
    public function actionStreets(){
        if ($get = \Yii::$app->request->get()) {
            $cityRef = Html::encode($get['city']);
            $needle = Html::encode($get['term']);

            $url = 'https://api.novaposhta.ua/v2.0/json/';
            $paramsArray = [
                "modelName" => "Address",
                "calledMethod" => "searchSettlementStreets",
                "methodProperties" => [
                    "StreetName" => $needle,
                    "SettlementRef" => $cityRef,
                    "Limit" => "10",
                ],
                "apiKey" => Yii::$app->params['novaPoshta']['apiKey']
            ];

            $result = Utils::CurTolNP($url, json_encode($paramsArray));
            $resultObj = json_decode($result);

            $streets = [];
            $i=0;
            foreach ($resultObj->data[0]->Addresses as $street) {
                $streets[$i]['value'] = $street->SettlementStreetRef;
                $streets[$i]['label'] = $street->Present;
                $i++;
            }

            echo json_encode($streets,JSON_UNESCAPED_UNICODE);
        }
    }
}