<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\User;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionLogin()
    {
        /* Что делать, если залогиненый пользователь случайно попал на страицу с формой логина  */
        if (!Yii::$app->user->isGuest){
            $this->redirectByRole();
        }

        $model = new LoginForm();

        /**
         * Проверка существаования пользователя. Если пользователя не существует (не совпадают логин/пароль),
         * в форму передаётся соотв. flash-сообщение. Иначе создаётся сессия пользователя.
         */
        if ($model->load(Yii::$app->request->post()) && $model->validate()){

            if (!$model->userExist()){
                Yii::$app->session->setFlash('wrong', '<p class="alert-danger login-wrong text-center">Wrong username or password</p>');
                $this->redirect('/');
            } else {
                $model->login();
                $this->redirectByRole();
            }
        } else {
            return $this->render('login', compact('model'));
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();
        $this->redirect('/');
    }

    /** На основании роли залогиненного пользователя осуществляется редирект на соотв. страницу
     * @return \yii\web\Response
     */
    private function redirectByRole(){
        $userRole = User::getUserRoleByUserId(Yii::$app->user->id);
        if ($userRole == "admin") {
            return $this->redirect("/admin/dashboard");
        } elseif ($userRole == "operator") {
            return $this->redirect("/leads/list");
        } elseif ($userRole == "storekeeper") {
            return $this->redirect("/storage/lead");
        } elseif ($userRole == "buh") {
            return $this->redirect("/storage/lead");
        }
    }
}
