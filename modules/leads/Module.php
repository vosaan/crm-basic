<?php

namespace app\modules\leads;

/**
 * leads module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\leads\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->layout = 'leads';
        parent::init();

        // custom initialization code goes here
    }
}
