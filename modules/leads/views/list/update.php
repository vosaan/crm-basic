<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Lead */

$this->title = 'Редактирование лида: ' . $lead->title;
$this->params['breadcrumbs'][] = ['label' => 'Leads', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $lead->title, 'url' => ['view', 'id' => $lead->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="lead-update">
    <div class="container">
        <h2><?= Html::encode($this->title) ?></h2>
    </div>
    <?= $this->render('_form', compact(
            'lead',
            'npForm',
            'nomenclature',
            'npDeliveryTech',
            'leadStatuses',
            'npPayerType',
            'npPaymentMethod',
            'npReturnShippingType',
            'npTtnNumber',
            'npDropshipperForm'
        )
    ) ?>

</div>
