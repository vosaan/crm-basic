<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\helpers\Utils;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $searchModel app\models\LeadSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Leads';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lead-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php //Utils::fOut($searchModel, 1)?>

<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'rowOptions' => function($model){
            if ($model->status_id == 1) {
                return ['class' => 'warning'];
            } elseif ($model->status_id == 2) {
                return ['class' => 'info'];
            } elseif ($model->status_id == 3) {
                return ['class' => 'danger'];
            } elseif ($model->status_id == 4) {
                return ['class' => 'success'];
            }
        },
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'id',
                'label' => 'ID лида',
                'contentOptions' => function($model){
                    if ($model->storageid !== 1){
                        return ['class'=>'big-border'];
                    } else {
                        return ['class'=>''];
                    }
                }
            ],
            [
                'attribute' => 'title',
                'value' => function($model){return Html::a($model->title, ['update', 'id' => $model->id]);},
                'format' => 'raw'
            ],
            'name',
            'second_name',
            'phone',
            'price',
            'domain',
            [
                'attribute' => 'status_id',
                'value' => function($model){
                    $status = \app\models\LeadStatus::find()->where(['id' => $model->status_id])->asArray()->one();
                    return ArrayHelper::getValue($status, 'name');

                }
            ],
            [
                'attribute' => 'responsible_user_id',
                'value' => function($model){
                    if (!$model->responsible_user_id) {
                        return '-';
                    } else {
                        $user = \app\models\User::find()->where(['id' => $model->responsible_user_id])->asArray()->one();
                        return ArrayHelper::getValue($user, 'username');
                    }
                },
                'label' => 'Ответственный оператор'

            ],
            'date_time',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
