<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\helpers\Utils;
use yii\helpers\ArrayHelper;
use yii\bootstrap\Alert;

/* @var $this yii\web\View */
/* @var $model app\models\Lead */
/* @var $form yii\widgets\ActiveForm */

//$this->registerJsFile('@web/js/np-script.js', ['depends' => 'yii\web\YiiAsset']);
$this->registerJsFile( '@web/js/jquery-ui.min.js', ['depends' => 'yii\web\JqueryAsset'] );
$this->registerJsFile( '@web/js/np-script.js', ['depends' => 'yii\web\JqueryAsset'] );
$this->registerCssFile( '@web/css/jquery-ui.css');
?>

<div class="lead-form">
    <div class="container">
        <div class="lead-wrapper">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <h4>
                        Дата регистрации лида: <?= Html::encode($lead->date_time) ?>
                    </h4>
                </div>
                <div class="col-lg-3 col-lg-offset-3 col-md-3 col-md-offset-3 col-sm-3 col-sm-offset-3 col-xs-12">
                    Ответственный за лид:
                    <strong>
                        <?php
                        $user = \app\models\User::find()->where(['id' => $lead->responsible_user_id])->asArray()->one();
                        echo ArrayHelper::getValue($user, 'username');
                        ?>
                    </strong>
                </div>
            </div>
        </div>

        <?php
            if ($lead->responsible_user_id != Yii::$app->user->id) {
                Alert::begin([
                    'options' => [
                        'class' => 'alert-warning',
                    ],
                ]);
                echo "<strong>Внимание!</strong> Этот лид уже обрабатывает " . ArrayHelper::getValue($user, 'username') . "!";
                Alert::end();
            }
        ?>




        <!-- Предупреждение о том, что товар не с нашего склада -->
        <?php
            if ($lead->storageid !== 1) {
                Alert::begin([
                    'options' => [
                        'class' => 'alert-danger',
                    ],
                ]);
                echo "<strong>Внимание!</strong> Товар находится на складе партнера!";
                Alert::end();
            }
        ?>
        <!-- //Предупреждение о том, что товар не с нашего склада -->

        <?php $form = ActiveForm::begin(); ?>

        <div class="lead-wrapper">
            <div class="row">
                <!-- Если товар не с основного скадала, то вывести ещё и цену закупки -->
                <?php if ($lead->storageid !== 1) : ?>

                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <?= $form->field($lead, 'product_id')->dropDownList(
                            ArrayHelper::map($nomenclature, 'id', 'name')
                        )->label('Товар') ?>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                        <?= $form->field($lead, 'quantity')->textInput(['maxlength' => true])->label('Количество') ?>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                        <?= $form->field($lead, 'price')->textInput(['maxlength' => true])->label('Цена') ?>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                        <?= $form->field($lead, 'purchase_price')->textInput(['maxlength' => true])->label('Цена закупки') ?>
                    </div>

                <?php else : ?>

                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                        <?= $form->field($lead, 'product_id')->dropDownList(
                            ArrayHelper::map($nomenclature, 'id', 'name')
                        )->label('Товар') ?>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                        <?= $form->field($lead, 'quantity')->textInput(['maxlength' => true])->label('Количество') ?>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                        <?= $form->field($lead, 'price')->textInput(['maxlength' => true])->label('Цена') ?>
                    </div>

                <?php endif; ?>
            </div>

            <hr>

            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <?= $form->field($lead, 'name')->textInput(['maxlength' => true]) ?>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <?= $form->field($lead, 'second_name')->textInput(['maxlength' => true]) ?>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <?= $form->field($lead, 'phone')->textInput(['maxlength' => true]) ?>
                </div>
            </div>

            <hr>

            <div class="row">
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                    <?= $form->field($lead, 'comment')->textarea(['rows' => 2]) ?>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <?= $form->field($lead, 'status_id')->dropDownList(
                        ArrayHelper::map($leadStatuses, 'id', 'name')
                    ) ?>
                </div>
            </div>
        </div>

        <!-- Форма для дропшиппера -->

        <?php if ($npDropshipperForm) : ?>
        <div class="lead-wrapper">
            <div class="row">
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                    <?= $form->field($npDropshipperForm, 'city')->textInput(['maxlength' => true])->label('Населенный пункт') ?>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <?= $form->field($npDropshipperForm, 'deliveryTech')->dropDownList(
                        ArrayHelper::map($npDeliveryTech, 'id', 'name')
                    )->label('Способ доставки') ?>
                </div>
            </div>



            <div class="row">
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                    <?= $form->field($npDropshipperForm, 'department')->textInput(['maxlength' => true])->label('Номер отделения') ?>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <?= $form->field($npDropshipperForm, 'street')->textInput(['maxlength' => true])->label('Улица') ?>
                </div>

                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                    <?= $form->field($npDropshipperForm, 'house')->textInput(['maxlength' => true])->label('Номер дома') ?>
                </div>

                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                    <?= $form->field($npDropshipperForm, 'flat')->textInput(['maxlength' => true])->label('Номер квартиры') ?>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <?= $form->field($npDropshipperForm, 'payerType')->dropDownList(
                        ArrayHelper::map($npPayerType, 'id', 'name')
                    )->label('Плательщик') ?>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <?= $form->field($npDropshipperForm, 'paymentMethod')->dropDownList(
                        ArrayHelper::map($npPaymentMethod, 'id', 'name')
                    )->label('Способ оплаты') ?>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <?= $form->field($npDropshipperForm, 'ttnNumber')->textInput(['maxlength' => true])->label('Номер экспресс-накладной') ?>
                </div>
            </div>
        </div>
        <?php endif; ?>

        <!-- //Форма для дропшиппера -->


        <!-- Форма для создания накладной Новой Почты -->

        <!-- Отображать форму создания накладной только если товар находится на основном (нашем) складе -->
        <?php if ($lead->storageid == 1) : ?>

            <!-- Вывод ошибки создания накладной -->
            <?php
                if (Yii::$app->session->getFlash('wrong')) {
                    Alert::begin([
                        'options' => [
                            'class' => 'alert-danger',
                        ],
                    ]);

                    echo "<h3>Error!</h3>";
                    echo "<p>" . "Reason: " . Yii::$app->session->getFlash('wrong') . "</p>";

                    Alert::end();
                }
            ?>
            <!-- //Вывод ошибки создания накладной -->


            <!-- Вывод ошибки API Новой почты -->
            <?php
                Alert::begin([
                    'options' => [
                        'class' => 'alert-danger hidden-alert-danger',
                    ],
                ]);
                echo '<strong>Внимание!</strong> Ошибка API "Новой почты": неверный ключ API либо сервис не отвечает!';
                Alert::end();
            ?>
            <!-- //Вывод ошибки API Новой почты -->

            <!-- Вывод данных о существовании или отсутствии ТТН к данному лиду -->
            <?php
                if (isset($npTtnNumber)) {

                    $ttnLink = 'https://my.novaposhta.ua/orders/printDocument/orders[]/'
                                . $npTtnNumber
                                . '/type/html/apiKey/'
                                . Yii::$app->params['novaPoshta']['apiKey'];

                    Alert::begin([
                        'options' => [
                            'class' => 'alert-success',
                        ],
                    ]);
                    echo "<strong>Внимание!</strong> На основании этого лида <a href='$ttnLink' target='_blank'>уже сформирована экспресс-накладная</a> для \"Новой почты\"";
                    Alert::end();
                } else {
                    Alert::begin([
                        'options' => [
                            'class' => 'alert-warning',
                        ],
                    ]);
                    echo '<strong>Внимание!</strong> На основании этого лида ещё не сформирована экспресс-накладная для "Новой почты';
                    Alert::end();
                }

            ?>
            <!-- //Вывод данных о существовании или отсутствии ТТН к данному лиду -->

            <div class="lead-wrapper">
                <label for="city city-input">Населенный пункт</label>
                <input type="text" name="city" id="city" class="form-control city-input">

                <?= $form->field($npForm, 'npServiceType')->dropDownList(
                    ArrayHelper::map($npDeliveryTech, 'id', 'name'),
                    ['class' => 'service-choice form-control', 'prompt' => '- Выберите способ доставки -', 'disabled' => true]
                )->label('Способ доставки') ?>

                <label for="department">Отделение</label>
                <select name="department" id="department" class="form-control" disabled></select>



                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <label for="street">Улица</label>
                        <input type="text" name="street" id="street" class="form-control" disabled>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        <?= $form->field($npForm, 'npRecipientHouse')->textInput(['maxlength' => true, 'disabled' => true])
                            ->label('Номер дома') ?>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" class="form-control">
                        <?= $form->field($npForm, 'npRecipientFlat')->textInput(['maxlength' => true, 'disabled' => true])
                            ->label('Квартира') ?>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        <?= $form->field($npForm, 'npWeight')->textInput(['maxlength' => true, 'disabled' => true])
                            ->label('Общий вес') ?>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        <?= $form->field($npForm, 'npVolumeGeneral')->textInput(['maxlength' => true, 'disabled' => true])
                            ->label('Объём посылки') ?>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        <?= $form->field($npForm, 'npSeatsAmount')->textInput(['maxlength' => true, 'disabled' => true])
                            ->label('Количество мест') ?>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        <?= $form->field($npForm, 'npDescription')->textInput(['maxlength' => true, 'disabled' => true])
                            ->label('Описание посылки') ?>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <?= $form->field($npForm, 'npPayerType')->dropDownList(
                            ArrayHelper::map($npPayerType, 'id', 'name'),
                            ['disabled' => true]
                        )->label('Плательщик') ?>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <?= $form->field($npForm, 'npPaymentMethod')->dropDownList(
                            ArrayHelper::map($npPaymentMethod, 'id', 'name'),
                            ['disabled' => true]
                        )->label('Способ оплаты') ?>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <?= $form->field($npForm, 'npReturnShippingType')->dropDownList(
                            ArrayHelper::map($npReturnShippingType, 'id', 'name'),
                            ['disabled' => true]
                        )->label('Обратная доставка') ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-lg-offset-8 col-md-4 col-md-offset-8 col-sm-4 col-sm-offset-8 col-xs-12">
                        <?= $form->field($npForm, 'npReturnShippingComment')->
                        textInput(['maxlength' => true, 'disabled' => true])->label('Комментарий к обратной доставке') ?>
                    </div>
                </div>

                <?= Html::activeHiddenInput($npForm, 'npRecipient', ['id' => 'np-department-ref', 'disabled' => true])?>
                <?= Html::activeHiddenInput($npForm, 'npRecipientCityName', ['id' => 'np-city-name', 'disabled' => true])?>
                <?= Html::activeHiddenInput($npForm, 'npRecipientAddressName', ['id' => 'np-department-num', 'disabled' => true])?>
            </div>
            <!-- //Форма для создания накладной Новой Почты -->

        <?php endif; ?>

        <div class="form-group">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
        </div>
        <?php ActiveForm::end(); ?>


    </div>
</div>
