<?php

namespace app\modules\leads\controllers;

use Yii;
use app\models\Lead;
use app\models\LeadSearch;
use app\models\LeadStatus;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\helpers\Utils;
use app\helpers\NpHelper;
use app\models\NpForm;
use app\models\NpDeliveryTech;
use app\models\NpPayerType;
use app\models\NpPaymentMethod;
use app\models\NpReturnShippingType;
use app\models\NpTtn;
use app\models\NpDropshipperForm;
use app\models\BuhNomenclature;
use yii\web\ForbiddenHttpException;
/**
 * ListController implements the CRUD actions for Lead model.
 */
class ListController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        if ($action->id == 'update') {
            $parameters = \yii::$app->getRequest()->getQueryParams();
            $model = $this->findModel($parameters['id']);

            /* Изменение статуса лида: если при открытии лида для редактирования его статус равен 1 ("новый лид"),
            то сменить его на статус 2 ("в работе") */
            if ($model->status_id == 1) {
                $model->status_id = 2;
                $model->save();
            }

            /* Изменение ответственного за лид */
            if (!$model->responsible_user_id) {
                $model->responsible_user_id = Yii::$app->user->id;
                $model->save();
            }

            if ($model->status_id == 5) {
                throw new ForbiddenHttpException('Лиды со статусом "В пути" не редактируются!');
            }
        }

        return parent::beforeAction($action); // TODO: Change the autogenerated stub
    }

    /**
     * Lists all Lead models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LeadSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Lead model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Updates an existing Lead model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        /* Модель лида (БД) */
        $lead = $this->findModel($id);

        $lead->scenario = "update";

        /* Справочник номенклатуры */
        $nomenclature = BuhNomenclature::find()->all();

        /* Модель формы ТТН Новой почты */
        $npForm = new NpForm();

        /* Статусы лида */
        $leadStatuses = LeadStatus::find()->all();

        /* Способ доставки */
        $npDeliveryTech = NpDeliveryTech::find()->orderBy('id DESC')->all();

        /* Тип плательщика */
        $npPayerType = NpPayerType::find()->all();

        /* Способ оплаты */
        $npPaymentMethod = NpPaymentMethod::find()->all();

        /* Обратная доставка */
        $npReturnShippingType = NpReturnShippingType::find()->all();

        /* Если товар не с основного склада - */
        if ($lead->storageid !== 1) {

            /*  */
            $dsTtn = NpTtn::find()->where(['lead_id' => $lead->id])->one();

            /*  */
            if ($dsTtn) {
                $npDropshipperForm = $this->setDropshipperFormFields($dsTtn);
            } else {
                $npDropshipperForm = new NpDropshipperForm();
            }
        }

        /* Значения полей по умолчанию */
        $npForm->npWeight = '0,1';
        $npForm->npVolumeGeneral = '0,004';
        $npForm->npSeatsAmount = '1';
        $npForm->npPayerType = 'Recipient';
        $npForm->npPaymentMethod = 'Cash';
        $npForm->npReturnShippingType = 'Money';
        $npTtnNumber = NpTtn::getTtnNumberByLeadId($lead->id);


        /* При редактировании данных лида (НЕ ТТН!) измененные данные записываются в БД */
        if ($lead->load(Yii::$app->request->post())){
            $lead->save();

            /* Если пришли какие-то данные из формы создания ТТН Новой почты */
            if (Yii::$app->request->post('NpForm')) {
                /* Атрибуты модели валидируются(?) и заполняются данными, полученными из формы */
                $npForm->load(Yii::$app->request->post());

                /* Название населенного пункта (поле, на которое в представлении повешен autocomplete для городов) */
                $city = Yii::$app->request->post('city');

                /* Название населенного пункта (поле, на которое в представлении повешен autocomplete для улиц) */
                $street = Yii::$app->request->post('street');

                /* Данные лида, данные формы Новой почты и название населенного пункта передаются в приватный
                метод (действие), который разруливает, какой хелпер вызвать для формирования и регистрации ТТН
                 */
                $message = $this->createTtn($lead, $npForm, $city, $street);

                if ($message == "Ok") {
                    $lead->status_id = 4;
                    $lead->save();
                    return $this->redirect( 'index');
                } else {
                    Yii::$app->session->setFlash('wrong', $message);
                    return $this->render('update', compact(
                        'lead',
                            'nomenclature',
                            'npForm',
                            'npDeliveryTech',
                            'leadStatuses',
                            'npPayerType',
                            'npPaymentMethod',
                            'npReturnShippingType'
                        )
                    );
                }
            }

            /* Если пришли данные из формы для ДропШипперов */
            if ($dsForm = Yii::$app->request->post('NpDropshipperForm')) {
                /* Если для этого лида нет данных по дропшипперу */
                if (!$dsTtn) {
                    /* Создать новыую запись в БД */
                    $this->setDropshipperTtnFields($lead->id, $dsForm, null);
                } else {
                    /* Иначе обновить запись */
                    $this->setDropshipperTtnFields(null, $dsForm, $dsTtn);
                }
            }

            return $this->redirect( 'index');
        } else {
            return $this->render('update', compact(
                'lead',
                    'nomenclature',
                    'npForm',
                    'npDeliveryTech',
                    'leadStatuses',
                    'npPayerType',
                    'npPaymentMethod',
                    'npReturnShippingType',
                    'npTtnNumber',
                    'npDropshipperForm'
                )
            );
        }
    }

    /**
     * Deletes an existing Lead model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Lead model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Lead the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Lead::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /** Осуществляет регистрацию ТТН в Новой почте и сохранение данных о сформированной ТТН (или об ошибках создания
     * ТТН) в БД
     * @param $lead
     * @param $npFormData
     * @param $city
     * @param $street
     */
    private function createTtn($lead, $npFormData, $city, $street){
        /* Если тип доставки "отделение - отделение" */
        if ($npFormData->npServiceType == "WarehouseWarehouse") {
            /* вызывается хелпер, который регистрирует ТТН в Новой почте */
            $result = NpHelper::toDepartment($lead, $npFormData, $city);
            /* если ТТН сформирована */
            if ($result['success'] == 'true') {
                /* вызывается хелпер, который сохраняет данные по накладной в БД */
                return NpHelper::saveTtn($lead->id, $result, $npFormData, $city);
            } else {
                /* иначе вызывается хелпер, который регистрирует ошибку создания ТТН */
                return NpHelper::saveTtnError($lead->id, $result);
            }
        }
        /* Если тип доставки "отделение - адрес" */
        elseif ($npFormData->npServiceType == "WarehouseDoors") {
            /* вызывается хелпер, который регистрирует ТТН в Новой почте */
            $result = NpHelper::toAddress($lead, $npFormData, $city, $street);
            /* если ТТН сформирована */
            if ($result['success'] == 'true') {
                /* вызывается хелпер, который сохраняет данные по накладной в БД */
                return NpHelper::saveTtn($lead->id, $result, $npFormData, $city, $street);
            } else {
                /* иначе вызывается хелпер, который регистрирует ошибку создания ТТН */
                return NpHelper::saveTtnError($lead->id, $result);
            }
        }
    }

    private function setDropshipperFormFields($dsTtn){
        $dsTtnData = new NpDropshipperForm();
        $dsTtnData->city = $dsTtn->city;
        $dsTtnData->department = $dsTtn->department;
        $dsTtnData->street = $dsTtn->street;
        $dsTtnData->house = $dsTtn->house;
        $dsTtnData->flat = $dsTtn->flat;
        $dsTtnData->payerType = $dsTtn->payer_type;
        $dsTtnData->paymentMethod = $dsTtn->payment_method;
        $dsTtnData->deliveryTech = $dsTtn->delivery_tech;
        $dsTtnData->ttnNumber = $dsTtn->ttn_number;
        return $dsTtnData;
    }

    private function setDropshipperTtnFields($leadId = null, $dsForm, $dsTtn = null){
        if (!$dsTtn) {
            $dsTtn = new NpTtn();
            $dsTtn->lead_id = $leadId;
        }

        $dsTtn->city = $dsForm['city'];
        $dsTtn->department = $dsForm['department'];
        $dsTtn->street = $dsForm['street'];
        $dsTtn->house = $dsForm['house'];
        $dsTtn->flat = $dsForm['flat'];
        $dsTtn->payer_type = $dsForm['payerType'];
        $dsTtn->payment_method = $dsForm['paymentMethod'];
        $dsTtn->delivery_tech = $dsForm['deliveryTech'];
        $dsTtn->ttn_number = $dsForm['ttnNumber'];
        return $dsTtn->save();
    }
}
