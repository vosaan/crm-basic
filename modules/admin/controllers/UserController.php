<?php

namespace app\modules\admin\controllers;

use app\helpers\Utils;
use Yii;
use app\models\User;
use app\modules\admin\models\UserForm;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\modules\admin\models\AuthItem;
use app\models\AuthAssignment;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => User::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $userForm = new UserForm();
        $userForm->scenario = 'create';
        $rolesArray = AuthItem::getAllRoles();

        if ($userForm->load(Yii::$app->request->post()) && $userForm->validate()) {
            $user = new User();

            $user->login = $userForm->login;
            $user->username = $userForm->username;
            $user->setPassword($userForm->password);
            $user->save();

            $auth = Yii::$app->authManager;
            $role = $auth->getRole($userForm->role);
            $auth->assign($role, $user->id);

            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'userForm' => $userForm,
                'rolesArray' => $rolesArray
            ]);
        }
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $user = $this->findModel($id);
        $userForm = new UserForm();
        $userForm->scenario = 'update';
        $rolesArray = AuthItem::getAllRoles();

        $userForm->login = $user->login;
        $userForm->username = $user->username;
        $userForm->role = User::getUserRoleByUserId($user->id);


        if ($userForm->load(Yii::$app->request->post()) && $userForm->validate()) {
            $user->login = $userForm->login;
            $user->username = $userForm->username;

            if ($userForm->password) {
                $user->setPassword($userForm->password);
            }

            $user->save();

            AuthAssignment::updateAll(['item_name' => $userForm->role], ['user_id' => $user->id]);

            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'userForm' => $userForm,
                'rolesArray' => $rolesArray
            ]);
        }
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        $userRole = AuthAssignment::find()->where(['user_id' => $id])->one();
        $userRole->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
