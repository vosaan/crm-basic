<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 05.09.2017
 * Time: 10:03
 */

namespace app\modules\admin\models;
use yii\base\Model;

class UserForm extends Model
{
    public $login;
    public $username;
    public $password;
    public $role;

    public function rules()
    {
        return [
            ['login', 'trim'],
            ['login', 'required'],
            ['login', 'unique', 'targetClass' => '\app\models\User', 'message' => 'This login has already been taken.', 'on' => 'create'],
            ['login', 'string', 'min' => 3, 'max' => 20],

            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'string', 'max' => 255],
            ['username', 'unique', 'targetClass' => '\app\models\User', 'message' => 'This username address has already been taken.', 'on' => 'create'],

            [['password'], 'required', 'on' => 'create'],
            [['password'], 'safe', 'on' => 'update'],
            ['password', 'string', 'min' => 6],

            [['role'], 'required', 'on' => 'create'],
            [['role'], 'safe', 'on' => 'update']
        ];
    }
}