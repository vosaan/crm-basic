<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Leads';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lead-index">
    <h1><?= Html::encode($this->title) ?></h1>

    <?= Html::a('<span class="glyphicon glyphicon-refresh"></span> Обновить стсусы посылок', '/admin/leadsstatistic/updatestatus', ['class' => 'btn btn-success'])?>

<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'id',
                'value' => function($model){return Html::a($model->id, ['view', 'id' => $model->id]);},
                'format' => 'raw'
            ],
            //'title',
            [
                'value' => function($model){
                    return Html::a(\app\models\BuhNomenclature::getTovarNameById($model->product_id), ['view', 'id' => $model->id]);
                },
                'attribute' => 'Товар',
                'format' => 'raw'
            ],
            //'name',
            //'second_name',
            // 'phone',
            // 'email:email',
             'price',
            // 'domain',
            // 'vcode',
            // 'status_id',
            // 'responsible_user_id',
            // 'comment:ntext',
            // 'date_time',
            // 'quantity',
            'city',
            'ttn',
            'ttnStatus',

            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
