<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($userForm, 'login')->textInput(['maxlength' => true]) ?>

    <?= $form->field($userForm, 'username')->textInput(['maxlength' => true]) ?>

    <?= $form->field($userForm, 'password')->passwordInput(['maxlength' => true]) ?>

    <?= $form->field($userForm, 'role')->dropDownList($rolesArray) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
