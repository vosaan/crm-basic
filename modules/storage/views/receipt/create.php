<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\storage\models\Receipt */

$this->title = 'Создать поступление';
$this->params['breadcrumbs'][] = ['label' => 'Поступления', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="receipt-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', compact('model', 'buhNomenclature')) ?>

</div>
