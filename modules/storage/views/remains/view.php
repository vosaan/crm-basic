<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\storage\models\Remains */

$this->title = $model->productName;
$this->params['breadcrumbs'][] = ['label' => 'Остатки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="remains-view">

    <h1><?= Html::encode($this->title) ?></h1>
    
    <?php /*
    <p>
        <?= Html::a('Обновить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить этот остаток?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    */
    ?>
    
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'productName',
            'quantity',
            'price_total',
        ],
    ]) ?>

</div>
