<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\modules\storage\models\Remains */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="remains-form">
    
 

    <?php $form = ActiveForm::begin(); ?>

    <div class="">
        <?= $form->field($model, 'product_id')->dropDownList(
            ArrayHelper::map($buhNomenclature, 'id', 'name')
        ) ?>
    </div>    
    
    
    <? /*= $form->field($model, 'product_id')->textInput() */ ?>

    <?= $form->field($model, 'quantity')->textInput() ?>

    <?= $form->field($model, 'price_total')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
