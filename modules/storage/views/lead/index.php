<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\LeadSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Лиды';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lead-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'title',
            'product_id',
            'name',
            'second_name',
            // 'phone',
            // 'email:email',
            // 'price',
            // 'domain',
            // 'vcode',
            // 'status_id',
            // 'responsible_user_id',
            // 'comment:ntext',
            // 'date_time',
            ['class' => 'yii\grid\ActionColumn',
              'template'=>'{view} {bttn}',
                'buttons'=>[
                      'bttn' => function ($url, $model) {
                        if ($model->ttn){
                            $url = 'https://my.novaposhta.ua/orders/printDocument/orders[]/' . $model->ttn . '/type/html/apiKey/' . Yii::$app->params['novaPoshta']['apiKey'];
                            return Html::a('<span class="glyphicon glyphicon-file"></span>', $url, [
                                    'title' => Yii::t('yii', 'ТТН'), 'target'=>'_blank'
                            ]);
                        }else{
                            return '';
                        }
                      }
                  ]
            ],
        ],
    ]); ?>
</div>
