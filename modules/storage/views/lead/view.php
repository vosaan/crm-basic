<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Lead */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Лиды', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lead-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php
            if ($model->ttn){
                $url = 'https://my.novaposhta.ua/orders/printDocument/orders[]/' . $model->ttn . '/type/html/apiKey/' . Yii::$app->params['novaPoshta']['apiKey'];
                echo Html::a('TTN', $url, ['title' => Yii::t('yii', 'ТТН'), 'target'=>'_blank', 'class' => 'btn btn-primary']);
            }
        ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'product_id',
            'name',
            'second_name',
            'phone',
            'email:email',
            'price',
            'domain',
            'vcode',
            'status_id',
            'responsible_user_id',
            'comment:ntext',
            'date_time',
        ],
    ]) ?>

</div>
