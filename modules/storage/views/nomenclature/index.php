<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\BuhNomenclatureSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Номенклатура';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="buh-nomenclature-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить товар', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            [
                'attribute' => 'storage_type_id',
                'value' => function($searchModel){
                    return \app\models\BuhStorageType::getStorageNameById($searchModel->storage_type_id);
                }
            ],
            ['class' => 'yii\grid\ActionColumn',
              'template'=>'{view} {update}'
            ],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
