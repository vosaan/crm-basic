<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\BuhNomenclature */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="buh-nomenclature-form">

    <?php $form = ActiveForm::begin(); ?>

    <? /*=$form->field($model, 'id')->textInput(['maxlength' => true]) */ ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?php if ($model->isNewRecord) : ?>
        <?= $form->field($model, 'storage_type_id')->dropDownList(
            ArrayHelper::map($storages, 'id', 'name'), ['prompt' => '- Выберите склад -']
        ) ?>
    <?php else : ?>
        <?= $form->field($model, 'storage_type_id')->dropDownList(
            ArrayHelper::map($storages, 'id', 'name')
        ) ?>
    <?php endif;?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
