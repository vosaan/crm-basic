<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\BuhNomenclature */

$this->title = 'Редактирование товара: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Номенклатура', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="buh-nomenclature-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'storages' => $storages
    ]) ?>

</div>
