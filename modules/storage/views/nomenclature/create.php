<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\BuhNomenclature */

$this->title = 'Добавиление товара';
$this->params['breadcrumbs'][] = ['label' => 'Номенклатура', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="buh-nomenclature-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'storages' => $storages
    ]) ?>

</div>
