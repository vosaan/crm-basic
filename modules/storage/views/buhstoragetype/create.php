<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\BuhStorageType */

$this->title = 'Create Buh Storage Type';
$this->params['breadcrumbs'][] = ['label' => 'Buh Storage Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="buh-storage-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
