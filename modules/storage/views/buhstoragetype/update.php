<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\BuhStorageType */

$this->title = 'Update Buh Storage Type: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Buh Storage Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="buh-storage-type-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
