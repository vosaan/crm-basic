<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\storage\models\Debit */

$this->title = 'Создать';
$this->params['breadcrumbs'][] = ['label' => 'Списание', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="debit-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', compact('model', 'buhNomenclature')) ?>

</div>
