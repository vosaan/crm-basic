<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\storage\models\Debit */

$this->title = $model->date_time;
$this->params['breadcrumbs'][] = ['label' => 'Списание', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="debit-view">

    <h1><?= Html::encode($this->title) ?></h1>


    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'date_time',
            'productName',
            'quantity',
            'amount',
            'comment:ntext',
            'lead_id:html',
        ],
    ]) ?>

</div>
