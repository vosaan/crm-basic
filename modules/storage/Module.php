<?php

namespace app\modules\storage;

use Yii;
use app\models\User;

/**
 * storage module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\storage\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        $userRole = User::getUserRoleByUserId(Yii::$app->user->id);
        if ($userRole != 'storage'){
            $this->layout = "@app/modules/buh/views/layouts/buh";
        }else{
            $this->layout = 'storage';
        }
        parent::init();

        // custom initialization code goes here
    }
}
