<?php

namespace app\modules\storage\models;

use Yii;
use app\models\BuhNomenclature;

/**
 * This is the model class for table "buh_debit".
 *
 * @property string $id
 * @property string $date_time
 * @property integer $product_id
 * @property integer $quantity
 * @property double $amount
 * @property string $comment
 * @property integer $lead_id
 */
class Debit extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'buh_debit';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_time'], 'safe'],
            [['product_id', 'quantity', 'lead_id'], 'integer'],
            [['amount'], 'number'],
            [['comment'], 'string'],
        ];
    }
    
    public function getbuh_nomenclature()
    {
        return $this->hasOne(BuhNomenclature::className(), ['id' => 'product_id']);
    }

    public function getProductName() {
        return $this->buh_nomenclature->name;
    }    

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_time' => 'Дата создания',
            'product_id' => 'ID Товара',
            'productName' => 'Товар',
            'quantity' => 'Колличество',
            'amount' => 'Сума',
            'comment' => 'Комметратий',
            'lead_id' => 'ID Лида',
        ];
    }
}
