<?php

namespace app\modules\storage\models;

use app\models\BuhNomenclature;
use Yii;

/**
 * This is the model class for table "buh_receipt".
 *
 * @property string $id
 * @property string $date_time
 * @property integer $product_id
 * @property integer $quantity
 * @property string $comment
 */
class Receipt extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'buh_receipt';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_time'], 'safe'],
            [['product_id', 'quantity'], 'integer'],
            [['amount'], 'number'],
            [['comment'], 'string'],
        ];
    }

    public function getbuh_nomenclature()
    {
        return $this->hasOne(BuhNomenclature::className(), ['id' => 'product_id']);
    }

    public function getProductName() {
        return $this->buh_nomenclature->name;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_time' => 'Дата',
            'productName' => 'Товар',
            'product_id' => 'Товар',
            'quantity' => 'Количество',
            'amount' => 'Сума',
            'comment' => 'Комментарий',
        ];
    }
}
