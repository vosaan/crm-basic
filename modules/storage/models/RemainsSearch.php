<?php

namespace app\modules\storage\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\storage\models\Remains;

/**
 * RemainsSearch represents the model behind the search form about `app\modules\storage\models\Remains`.
 */
class RemainsSearch extends Remains
{
    public $productName;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'product_id', 'quantity'], 'integer'],
            [['price_total'], 'number'],
            [['productName'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Remains::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $query->joinWith(['buh_nomenclature']);

        $dataProvider->setSort([
            'attributes' => [
                'id',
                'productName' => [
                    'asc' => ['buh_nomenclature.name' => SORT_ASC],
                    'desc' => ['buh_nomenclature.name' => SORT_DESC],
                    'label' => 'Товар'
                ],
                'quantity',
                'price_total'
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'product_id' => $this->product_id,
            'quantity' => $this->quantity,
            'price_total' => $this->price_total,
        ]);

        // Фильтр по стране
        if ($this->productName) {
            $query->andFilterWhere(['like', 'buh_nomenclature.name', $this->productName]);
        }

        return $dataProvider;
    }
}
