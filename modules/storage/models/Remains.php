<?php

namespace app\modules\storage\models;

use app\models\BuhNomenclature;
use Yii;

/**
 * This is the model class for table "buh_storage".
 *
 * @property string $id
 * @property integer $product_id
 * @property integer $quantity
 * @property double $price_total
 */
class Remains extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'buh_storage';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'quantity'], 'integer'],
            [['price_total'], 'number'],
        ];
    }

    public function getbuh_nomenclature()
    {
        return $this->hasOne(BuhNomenclature::className(), ['id' => 'product_id']);
    }

    public function getProductName() {
        return $this->buh_nomenclature->name;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'productName' => 'Товар',
            'product_id' => 'Товар',
            'quantity' => 'Количество на складе',
            'price_total' => 'Цена остатка',
        ];
    }
}
