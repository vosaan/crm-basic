<?php

namespace app\modules\storage\controllers;
use app\helpers\Storage;
use Yii;
use app\models\BuhNomenclature;
use app\modules\storage\models\Debit;
use app\modules\storage\models\DebitSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DebitController implements the CRUD actions for Debit model.
 */
class DebitController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Debit models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DebitSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Debit model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        $dd = $this->findModel($id);
        
        if ($dd->lead_id){
            $dd->lead_id = '<a href="/storage/lead/view?id=' . $dd->lead_id . '">просмотреть лид</a>';
        }else{
            $dd->lead_id = '-';
        }
        
        return $this->render('view', [
            'model' => $dd,
        ]);
    }

    /**
     * Creates a new Debit model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Debit();
        
        $buhNomenclature = BuhNomenclature::find()->all();

        if ($model->load(Yii::$app->request->post())) {
            $postForm = Yii::$app->request->post();
            
            //списываем со склада
            $summ = Storage::debitStorage($postForm['Debit']['product_id'], $postForm['Debit']['quantity']);
            $model->amount = $summ;
            
            $model->save();
            
            return $this->redirect(['index']);
        } else {
            return $this->render('create', compact('model', 'buhNomenclature'));
        }
    }

    /**
     * Updates an existing Debit model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Debit model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Debit model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Debit the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Debit::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
