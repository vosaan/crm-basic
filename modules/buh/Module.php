<?php

namespace app\modules\buh;

/**
 * buh module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\buh\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->layout = 'buh';
		parent::init();
		

        // custom initialization code goes here
    }
}
