<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\buh\models\Expenditure */

$this->title = 'Создать статью расходов';
$this->params['breadcrumbs'][] = ['label' => 'Статьи расходов', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="expenditure-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
