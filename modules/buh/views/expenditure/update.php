<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\buh\models\Expenditure */

$this->title = 'Обновить статью расходов: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Статьи расходов', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Обновить';
?>
<div class="expenditure-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
