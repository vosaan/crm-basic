<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\buh\models\Costs */

$this->title = 'Создать расход';
$this->params['breadcrumbs'][] = ['label' => 'Расходы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="costs-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', compact('model', 'expenditure')) ?>

</div>
