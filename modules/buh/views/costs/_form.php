<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\modules\buh\models\Costs */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="costs-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'date_time')->widget(\yii\jui\DatePicker::classname(), [
        'language' => 'ru',
        'dateFormat' => 'yyyy-MM-dd',
    ]) ?>

    <div class="">
        <?= $form->field($model, 'expenditure_id')->dropDownList(
            ArrayHelper::map($expenditure, 'id', 'name')
        ) ?>
    </div>

    <?= $form->field($model, 'sum')->textInput() ?>

    <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
