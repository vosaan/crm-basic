<?php

namespace app\modules\buh\models;

use Yii;

/**
 * This is the model class for table "buh_costs".
 *
 * @property string $id
 * @property string $date_time
 * @property integer $expenditure_id
 * @property integer $year
 * @property integer $month
 * @property double $sum
 * @property string $comment
 */
class Costs extends \yii\db\ActiveRecord
{

    public static function getMonthArray(){
        return $monthArray = [
            "1"  => "Январь",
            "2"  => "Февраль",
            "3"  => "Март",
            "4"  => "Апрель",
            "5"  => "Май",
            "6"  => "Июнь",
            "7"  => "Июль",
            "8"  => "Август",
            "9"  => "Сентябрь",
            "10" => "Октябрь",
            "11" => "Ноябрь",
            "12" => "Декабрь",
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'buh_costs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_time'], 'safe'],
            [['expenditure_id'], 'integer'],
            [['sum'], 'number'],
            [['comment'], 'string'],
        ];
    }

    public function getbuh_expenditure()
    {
        return $this->hasOne(Expenditure::className(), ['id' => 'expenditure_id']);
    }

    public function getExpenditureName() {
        return $this->buh_expenditure->name;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_time' => 'Дата создания',
            'expenditure_id' => 'Статья',
            'expenditureName' => 'Статья',
            'sum' => 'Сумма',
            'comment' => 'Комментарий',
        ];
    }
}
