<?php

namespace app\modules\buh\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * CostsSearch represents the model behind the search form about `app\modules\buh\models\Costs`.
 */
class CostsSearch extends Costs
{

    public $expenditureName;
    public $date_from;
    public $date_to;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'expenditure_id'], 'integer'],
            [['date_time', 'comment'], 'safe'],
            [['sum'], 'number'],
            [['expenditureName'], 'safe'],
            [['date_from','date_to'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Costs::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $query->joinWith(['buh_expenditure']);

        $dataProvider->setSort([
            'attributes' => [
                'id',
                'date_time',
                'expenditureName' => [
                    'asc' => ['buh_expenditure.name' => SORT_ASC],
                    'desc' => ['buh_expenditure.name' => SORT_DESC],
                    'label' => 'Статья'
                ],
                'sum',
                'comment'
            ],
            'defaultOrder' => [
                'date_time' => SORT_DESC
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'date_time' => $this->date_time,
            'expenditure_id' => $this->expenditure_id,
            'sum' => $this->sum,
        ]);

        $query->andFilterWhere(['like', 'comment', $this->comment]);

        // Фильтр
        if ($this->expenditureName) {
            $query->andFilterWhere(['like', 'buh_expenditure.name', $this->expenditureName]);
        }
        if ($this->date_from){
            $query->andFilterWhere(['>=', 'date_time', $this->date_from])
            ->andFilterWhere(['<=', 'date_time', $this->date_to]);
        }

        /*if ($this->monthName) {
            $monthIn = [];
            foreach (Costs::getMonthArray() as $keyMonth => $valMonth) {
                $pos = strpos($valMonth, $this->monthName);
                if ($pos === false) {
                    // echo 'Подстрока не найдена';
                }else{
                    $monthIn[] = $keyMonth;
                }
            }
            if (count($monthIn)){
                $query->andFilterWhere(['in', 'month', $monthIn]);
            }
        }*/

        return $dataProvider;
    }
}
