<?php

use yii\db\Migration;

class m170928_124712_modif_table_buh_receipt extends Migration
{
    public function safeUp()
    {
        $this->execute("
            ALTER TABLE `buh_receipt` ADD `amount` FLOAT(10,2) NOT NULL DEFAULT '0' COMMENT 'Сума' AFTER `quantity`;
        ");
    }

    public function safeDown()
    {
        echo "m170928_124712_modif_table_buh_receipt cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170928_124712_modif_table_buh_receipt cannot be reverted.\n";

        return false;
    }
    */
}
