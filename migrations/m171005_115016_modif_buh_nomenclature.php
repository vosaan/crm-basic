<?php

use yii\db\Migration;

class m171005_115016_modif_buh_nomenclature extends Migration
{
    public function safeUp()
    {
        $this->execute("
            ALTER TABLE `buh_nomenclature` ADD COLUMN `storage_type_id` INT(10) NOT NULL DEFAULT '1' AFTER `name`;
        ");
    }

    public function safeDown()
    {
        $this->execute("
            ALTER TABLE `buh_nomenclature` DROP COLUMN `storage_type_id`;
        ");
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171005_115016_modif_buh_nomenclature cannot be reverted.\n";

        return false;
    }
    */
}
