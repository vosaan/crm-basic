<?php

use yii\db\Migration;

class m170925_081157_modif_coast_table extends Migration
{
    public function safeUp()
    {
        $this->execute("
            ALTER TABLE `buh_costs` DROP `year`, DROP `month`;
            ALTER TABLE `buh_costs` CHANGE `date_time` `date_time` DATE NULL DEFAULT NULL COMMENT 'Дата создания';
        ");
    }

    public function safeDown()
    {
        echo "m170925_081157_modif_coast_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170925_081157_modif_coast_table cannot be reverted.\n";

        return false;
    }
    */
}
