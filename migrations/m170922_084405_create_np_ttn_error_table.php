<?php

use yii\db\Migration;

/**
 * Handles the creation of table `np_ttn_error`.
 */
class m170922_084405_create_np_ttn_error_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('np_ttn_error', [
            'id' => $this->primaryKey(),
            'lead_id' => $this->integer()->notNull()->notNull(),
            'registered_time' => $this->timestamp()->notNull(),
            'reason' => $this->string(255)->notNull()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('np_ttn_error');
    }
}
