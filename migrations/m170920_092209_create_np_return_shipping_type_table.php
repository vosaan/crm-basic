<?php

use yii\db\Migration;

/**
 * Handles the creation of table `np_return_shipping_type`.
 */
class m170920_092209_create_np_return_shipping_type_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('np_return_shipping_type', [
            'id' => $this->primaryKey(),
            'id' => $this->string(20)->notNull(),
            'name' => $this->string(50)->notNull()
        ]);

        $this->insert('np_return_shipping_type', [
            'id' => 'Documents',
            'name' => 'Документи'
        ]);

        $this->insert('np_return_shipping_type', [
            'id' => 'Money',
            'name' => 'Грошовий переказ'
        ]);

        $this->insert('np_return_shipping_type', [
            'id' => 'No',
            'name' => 'Немає зворотньої досавки'
        ]);

        $this->insert('np_return_shipping_type', [
            'id' => 'Other',
            'name' => 'Інше'
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('np_return_shipping_type');
    }
}
