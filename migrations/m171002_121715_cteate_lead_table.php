<?php

use yii\db\Migration;

class m171002_121715_cteate_lead_table extends Migration
{
    public function safeUp()
    {
        $this->execute(
        "
            DROP TABLE `lead`;
            
            CREATE TABLE `lead` (
              `id` int(11) NOT NULL COMMENT 'ID лида',
              `title` varchar(255) NOT NULL COMMENT 'Название товара',
              `product_id` smallint(5) DEFAULT NULL COMMENT 'ID товара',
              `name` varchar(255) NOT NULL COMMENT 'Имя заказчика',
              `second_name` varchar(255) DEFAULT NULL COMMENT 'Фамилия заказчика',
              `phone` varchar(20) NOT NULL COMMENT 'Номер телефона заказчика',
              `email` varchar(100) DEFAULT NULL COMMENT 'E-mail заказчика',
              `price` smallint(5) UNSIGNED NOT NULL COMMENT 'Цена товара',
              `domain` varchar(100) NOT NULL COMMENT 'Адрес лендинга',
              `vcode` varchar(50) DEFAULT NULL COMMENT 'vcode (для Octotracker)',
              `status_id` tinyint(1) UNSIGNED NOT NULL DEFAULT '1' COMMENT 'Статус лида',
              `responsible_user_id` tinyint(3) UNSIGNED DEFAULT NULL COMMENT 'ID оператора, отвестсвенного за лид',
              `comment` text COMMENT 'Комметратий к лиду',
              `date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Дата и время поступления лида',
              `quantity` tinyint(3) UNSIGNED NOT NULL DEFAULT '1'
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
            
            
            ALTER TABLE `lead` ADD PRIMARY KEY (`id`);
			
			ALTER TABLE `lead` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID лида', AUTO_INCREMENT=1;
            "
        );
    }

    public function safeDown()
    {
        echo "m171002_121715_cteate_lead_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171002_121715_cteate_lead_table cannot be reverted.\n";

        return false;
    }
    */
}
