<?php

use yii\db\Migration;

class m171005_144519_modif_lead extends Migration
{
    public function safeUp()
    {
        $this->execute("
            ALTER TABLE `lead` ADD COLUMN `purchase_price` FLOAT UNSIGNED NULL COMMENT 'Закупочная цена для Drop Shippers' AFTER `price`;
        ");
    }

    public function safeDown()
    {
        $this->execute("
            ALTER TABLE `lead` DROP COLUMN `purchase_price`;
        ");
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171005_144519_modif_lead cannot be reverted.\n";

        return false;
    }
    */
}
