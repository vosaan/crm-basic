<?php

use yii\db\Migration;

/**
 * Handles the creation of table `np_ttn`.
 */
class m170925_130114_create_np_ttn_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('np_ttn', [
            'id' => $this->primaryKey(),
            'lead_id' => $this->integer()->notNull(),
            'city' => $this->string(255)->notNull(),
            'department' => $this->integer(4),
            'street' => $this->string(100),
            'house' => $this->string(5),
            'flat' => $this->integer(5),
            'ttn_number' => $this->bigInteger()->unsigned()->notNull(),
            'estimated_delivery_date' => $this->string(10)->notNull(),
            'cost' => $this->integer(4)->notNull(),
            'np_status_code' => $this->integer(3)->defaultValue(1)->notNull(),
            'np_status' => $this->string(255)->notNull(),
            'payer_type' => $this->string()->notNull(),
            'payment_method' => $this->string()->notNull(),
            'delivery_tech' => $this->string()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('np_ttn');
    }
}
