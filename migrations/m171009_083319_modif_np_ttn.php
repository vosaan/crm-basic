<?php

use yii\db\Migration;

class m171009_083319_modif_np_ttn extends Migration
{
    public function safeUp()
    {
        $this->execute("
            ALTER TABLE `np_ttn` ALTER `ttn_number` DROP DEFAULT;
            ALTER TABLE `np_ttn` CHANGE COLUMN `ttn_number` `ttn_number` BIGINT(20) UNSIGNED NULL AFTER `flat`;

            ALTER TABLE `np_ttn` ALTER `estimated_delivery_date` DROP DEFAULT;
            ALTER TABLE `np_ttn` CHANGE COLUMN `estimated_delivery_date` `estimated_delivery_date` VARCHAR(10) NULL AFTER `ttn_number`;
            
            ALTER TABLE `np_ttn` ALTER `np_status` DROP DEFAULT;
            ALTER TABLE `np_ttn` CHANGE COLUMN `np_status` `np_status` VARCHAR(255) NULL AFTER `np_status_code`;
            
            ALTER TABLE `np_ttn` ALTER `cost` DROP DEFAULT;
            ALTER TABLE `np_ttn` CHANGE COLUMN `cost` `cost` INT(4) NULL AFTER `estimated_delivery_date`;
        ");
    }

    public function safeDown()
    {
        $this->execute("
            ALTER TABLE `np_ttn` ALTER `ttn_number` DROP DEFAULT;
            ALTER TABLE `np_ttn` CHANGE COLUMN `ttn_number` `ttn_number` BIGINT(20) UNSIGNED NOT NULL AFTER `flat`;
            
            ALTER TABLE `np_ttn` ALTER `estimated_delivery_date` DROP DEFAULT;
            ALTER TABLE `np_ttn` CHANGE COLUMN `estimated_delivery_date` `estimated_delivery_date` VARCHAR(10) NOT NULL AFTER `ttn_number`;
            
            ALTER TABLE `np_ttn` ALTER `np_status` DROP DEFAULT;
            ALTER TABLE `np_ttn` CHANGE COLUMN `np_status` `np_status` VARCHAR(255) NOT NULL AFTER `np_status_code`;
            
            ALTER TABLE `np_ttn` ALTER `cost` DROP DEFAULT;
            ALTER TABLE `np_ttn` CHANGE COLUMN `cost` `cost` INT(4) NOT NULL AFTER `estimated_delivery_date`;
        ");
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171009_083319_modif_np_ttn cannot be reverted.\n";

        return false;
    }
    */
}
