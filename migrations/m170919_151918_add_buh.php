<?php

use yii\db\Migration;

class m170919_151918_add_buh extends Migration
{
    public function safeUp()
    {
        $this->execute("
            INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES ('buh', 1, 'Buhgalter', NULL, NULL, 1505808949, 1505808949);
            INSERT INTO `user` (`id`, `login`, `username`, `password`) VALUES (23, 'buh', 'buh', '$2y$13$WN9S3r/GwKHH5vgIqRHDoudDktBnbAREbCgI9TfXGlucujJ1QH0bK');
            INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES ('buh', '23', 1505809030);
            
            CREATE TABLE `buh_expenditure` (
            `id` int(10) UNSIGNED NOT NULL,
            `name` varchar(255) NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
            
            ALTER TABLE `buh_expenditure` ADD PRIMARY KEY (`id`);
            ALTER TABLE `buh_expenditure` MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
            
            CREATE TABLE `buh_costs` (
            `id` int(10) UNSIGNED NOT NULL,
            `date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Дата создания',
            `expenditure_id` int(11) DEFAULT NULL COMMENT 'ID статьи расходов',
            `year` int(4) DEFAULT NULL COMMENT 'год расхода',
            `month` int(2) DEFAULT NULL COMMENT 'месяц расхода',
            `sum` float(10,2) UNSIGNED NOT NULL DEFAULT '0.00' COMMENT 'Сума расхода',
            `comment` text COMMENT 'Комметратий'
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
            
            ALTER TABLE `buh_costs` ADD PRIMARY KEY (`id`);
            ALTER TABLE `buh_costs` MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
            COMMIT;
        ");
    }

    public function safeDown()
    {
        echo "m170919_151918_add_buh cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170919_151918_add_buh cannot be reverted.\n";

        return false;
    }
    */
}
