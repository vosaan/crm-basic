<?php

use yii\db\Migration;

class m170914_112721_modif_size_name extends Migration
{
    public function safeUp()
    {
        $this->execute("
            ALTER TABLE `buh_nomenclature` CHANGE `name` `name` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;
        ");
    }

    public function safeDown()
    {
        echo "m170914_112721_modif_size_name cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170914_112721_modif_size_name cannot be reverted.\n";

        return false;
    }
    */
}
