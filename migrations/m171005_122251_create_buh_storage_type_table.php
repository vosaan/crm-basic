<?php

use yii\db\Migration;

/**
 * Handles the creation of table `buh_storage_type`.
 */
class m171005_122251_create_buh_storage_type_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->execute("
            CREATE TABLE `buh_storage_type` (
                `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
                `name` VARCHAR(50) NOT NULL,
                `comment` VARCHAR(255) NULL,
                PRIMARY KEY (`id`)
            ) COLLATE='utf8_general_ci' ENGINE=InnoDB;
                    
            INSERT INTO `buh_storage_type` (`name`,`comment`) VALUES ('Основной','Основной склад');
            INSERT INTO `buh_storage_type` (`name`,`comment`) VALUES ('Вася (drop shipper)','Drop shipper Вася из Новомосковска (тел. 098 636 59 72)');
        ");
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('buh_storage_type');
    }
}
