    <?php

use yii\db\Migration;

class m170914_141344_add_table_receipt extends Migration
{
    public function safeUp()
    {
        $this->execute("
            CREATE TABLE `buh_receipt` (
              `id` int(10) UNSIGNED NOT NULL,
              `date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Дата списания',
              `product_id` int(11) DEFAULT NULL COMMENT 'ID товара',
              `quantity` int(11) DEFAULT '0' COMMENT 'Количесвто',
              `comment` text COMMENT 'Комметратий'
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

            ALTER TABLE `buh_receipt` ADD PRIMARY KEY (`id`);
            ALTER TABLE `buh_receipt` MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
            COMMIT;
        ");
    }

    public function safeDown()
    {
        echo "m170914_141344_add_table_receipt cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170914_141344_add_table_receipt cannot be reverted.\n";

        return false;
    }
    */
}
