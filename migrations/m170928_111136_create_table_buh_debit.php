<?php

use yii\db\Migration;

class m170928_111136_create_table_buh_debit extends Migration
{
    public function safeUp()
    {
        $this->execute("
            CREATE TABLE `buh_debit` (
              `id` int(10) UNSIGNED NOT NULL,
              `date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Дата создания',
              `product_id` int(11) DEFAULT NULL COMMENT 'ID продукта',
              `quantity` int(11) DEFAULT NULL COMMENT 'Колличество продуктов',
              `amount` float(10,2) UNSIGNED NOT NULL DEFAULT '0.00' COMMENT 'Сума списания',
              `comment` text COMMENT 'Комметратий',
              `lead_id` int(11) DEFAULT NULL COMMENT 'ID лида'
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

            ALTER TABLE `buh_debit`
              ADD PRIMARY KEY (`id`);
              
            ALTER TABLE `buh_debit`
              MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
        ");
    }

    public function safeDown()
    {
        echo "m170928_111136_create_table_buh_debit cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170928_111136_create_table_buh_debit cannot be reverted.\n";

        return false;
    }
    */
}
