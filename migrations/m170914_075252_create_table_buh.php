<?php

use yii\db\Migration;

class m170914_075252_create_table_buh extends Migration
{
    public function safeUp()
    {
        $this->execute("
            CREATE TABLE `buh_nomenclature` (
              `id` int(10) UNSIGNED NOT NULL,
              `name` varchar(20) NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ");
        $this->execute("
            CREATE TABLE `buh_storage` (
              `id` int(10) UNSIGNED NOT NULL,
              `product_id` int(11) DEFAULT NULL COMMENT 'ID товара',
              `quantity` int(11) DEFAULT '0' COMMENT 'Количесвто',
              `price_total` float(10,2) UNSIGNED NOT NULL DEFAULT '0.00' COMMENT 'Цена товара'
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ");
        $this->execute("
            ALTER TABLE `buh_nomenclature`  ADD PRIMARY KEY (`id`);
            ALTER TABLE `buh_storage` ADD PRIMARY KEY (`id`);
            ALTER TABLE `buh_nomenclature` MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
            ALTER TABLE `buh_storage` MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
        ");
    }

    public function safeDown()
    {
        echo "m170914_075252_create_table_buh cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170914_075252_create_table_buh cannot be reverted.\n";

        return false;
    }
    */
}
